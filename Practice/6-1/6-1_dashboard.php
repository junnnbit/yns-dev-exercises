<?php
    session_start();
    if (empty($_SESSION['username']) && empty($_SESSION['password'])) {
        header('Location: 6-1_login.php');
    }

    $basename_server = basename($_SERVER['SCRIPT_NAME']);
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <title>6-1 Exercise</title>
        <!-- JQuery package -->
        <script src="../assets/jQuery/jquery.js"></script>
        <!-- swal package -->
        <script src="../assets/js/swal.js"></script>
        <!-- Packages -->
        <link href="../assets/css/styles.css" rel="stylesheet" />
        <link href="../assets/css/custom.css" rel="stylesheet" />
        <link href="../assets/bootstrap/bootstrap.min.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-yns">
            <!-- Navbar Brand-->
            <a class="navbar-brand ps-3" href="6-1_dashboard.php"><img src="../assets/img/YNS_logo.png" style="width: 20%;"></a>
            <!-- Sidebar Toggle-->
            <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!"><i class="fas fa-bars"></i></button>
            <!-- Navbar Search-->
            <div class="d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0">
            </div>
            <!-- Navbar-->
            <ul class="navbar-nav ms-auto ms-md-0 me-3 me-lg-4">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="javascript:location.replace('6-1_logout.php');">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-light" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav sidebar">
                            <!-- User Info -->
                            <div class="user-info row">
                                <div class="user-container">
                                    <img src="<?= $_SESSION['image'] ?>" width="48" height="48" alt="User" />
                                </div>
                                <div class="info-container div-col">
                                    <div class="info-header">Welcome,</div>
                                    <div class="name"><?= $_SESSION['username'] ?></div>
                                </div>
                            </div>
                            <!-- #User Info -->
                            <div class="sb-sidenav-menu-heading">Menu</div>
                            <a class="nav-link <?= $basename_server == '6-1_dashboard.php' ? 'active' : ''?>" href="6-1_dashboard.php">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Dashboard
                            </a>
                            <a class="nav-link <?= $basename_server == '6-1_quiz.php' ? 'active' : ''?>" href="6-1_quiz.php">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Quiz
                            </a>        
                        </div>
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid px-4">
                        <h3 class="mt-4">Dashboard</h3>
                        <div style="padding-top:20px;"></div>
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table me-1"></i>
                                Quiz Results History
                            </div>
                            <div class="card-body">
                                <table class="resultDataTables table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Result ID</th>
                                            <th>Result</th>
                                            <th>Equivalent Percentage</th>
                                            <th>Remarks</th>
                                            <th>Date Taken</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="../assets/js/scripts.js"></script>
        <link href="https://cdn.datatables.net/1.11.2/css/jquery.dataTables.min.css" rel="stylesheet">
        <script src="https://cdn.datatables.net/1.11.2/js/jquery.dataTables.min.js"></script>
    </body>
</html>
<script>
$(function () {
    resultsDataTablesFunc();
});

function resultsDataTablesFunc() {
    var table = 'table.resultDataTables';
    var resultsDT = $(table).DataTable({
        ajax: '../ajax/view_quiz_results.php',
        'columns':[
            { 'data': 'results_id', 'bSortable': false, render: function ( data,type,row ){
                return 'Result #' + data.padStart(5, '0');
            }},
            { 'data': '', 'bSortable': false, render: function ( data,type,row ){
                return row.results_result / row.quiz_count + '/' + row.quiz_count;
            }},
            { 'data': 'results_result', 'bSortable': false, render: function ( data,type,row ){
                return data + '%';
            }},
            { 'data': 'results_remarks', 'bSortable': false, render: function ( data,type,row ){
                return data;
            }},
            { 'data': 'results_date_taken', 'bSortable': false, render: function ( data,type,row ){
                return data;
            }},
            { 'data': '', 'bSortable': false, render: function ( data,type,row ){
                return '<button class="btn btn-small btn-info viewResultDetails">View</button>';
            }}
        ]
    });

    $(resultsDT.table().container()).on('click', 'button.viewResultDetails', function () {
        var cell_clicked    = resultsDT.cell(this).data(),
            row_clicked     = $(this).closest('tr'),
            row_object      = resultsDT.row(row_clicked).data();

        var result_id       = row_object['results_id'];

        var redirectPage = "6-1_quizResultDetailed.php?id=" + result_id;
        location.assign(redirectPage);

    });
}
</script>
