<?php
    session_start();
    if(!empty($_SESSION['username']) && !empty($_SESSION['password'])){
        header('Location: 6-1_dashboard.php');
    }
?>

<style>
    .fit-image{
        width: 100%;
        object-fit: cover;
        
    }

    .div_spacing-10{
        padding-top:10px;
    }
</style>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <title>6-1 Exercise</title>

        <!-- JQuery package -->
        <script src="../assets/jQuery/jquery.js"></script>

        <!-- swal package -->
        <script src="../assets/js/swal.js"></script>
        
        <!-- Bootstrap -->
        <link href="../assets/bootstrap/bootstrap.min.css" rel="stylesheet">
        <script src="../assets/bootstrap/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="row" style="margin: 0px !important;">
        <div class="col-md-12" style="height: 10%; background-color: #db0c34;">
            <img src="../assets/img/YNS_logo.png" style="width: 5%;">
            <div style="padding-right:5px;padding-top:7px;float:right">
                <div class="form-inline login_div">
                    <label class="m-2" for="login_username" style="color:white">Username</label>
                    <input type="text" class="form-control login_username required m-2" id="username" required>
                    <div></div>

                    <label class="m-2" for="login_password" style="color:white">Password</label>
                    <input type="password" class="form-control login_password required m-2" id="pass" required>
                    <div></div>

                    &nbsp;&nbsp;&nbsp;
                    <button type="submit" class="btn btn-sm loginBtn">Log-in</button>
                </div>
            
            </div>
        </div>
        <div class="col-md-6">
            <img src="../assets/img/globe_2.jpg" class="fit-image" style="border-right: 1px solid black;"> 
        </div>
        <div class="col-md-6" style="padding-left: 0px;">
            <div class="col-md-12" style="text-align: center; padding-top: 25px;">
                <h4>Don't have an account yet? Sign-up now!</h4>
            </div>
            <div class="row reg_div" style="margin: 0px !important;">
                <div class="col-md-12" style="padding-top:22px;"></div>

                <!-- First Name -->
                <div class="col-md-2"></div>
                <div class="col-md-3" style="padding-top:5px;">
                    <label for="firstname"><small style="color:red;">* </small>First Name</label>
                </div>
                <div class="col-md-5">
                    <input type="text" class="form-control reg_firstname required" id="firstname">
                </div>
                <div class="col-md-1"  style="padding-top:8px;"></div>
                <!-- End of First Name -->

                <div class="col-md-12 div_spacing-10"></div>

                <!-- Middle Name -->
                <div class="col-md-2"></div>
                <div class="col-md-3" style="padding-top:5px;">
                    <label for="middlename">Middle Name</label>
                </div>
                <div class="col-md-5">
                    <input type="text" class="form-control reg_middlename" id="middlename">
                </div>
                <div class="col-md-1"  style="padding-top:8px;"></div>
                <!-- End of Middle Name -->

                <div class="col-md-12 div_spacing-10"></div>

                <!-- Last Name -->
                <div class="col-md-2"></div>
                <div class="col-md-3" style="padding-top:5px;">
                    <label for="lastname"><small style="color:red;">* </small>Last Name</label>
                </div>
                <div class="col-md-5">
                    <input type="text" class="form-control reg_lastname required" id="lastname">
                </div>
                <div class="col-md-1"  style="padding-top:8px;"></div>
                <!-- End of Last Name -->

                <div class="col-md-12 div_spacing-10"></div>

                <!-- Birthdate -->
                <div class="col-md-2"></div>
                <div class="col-md-3" style="padding-top:5px;">
                    <label for="birthdate"><small style="color:red;">* </small>Birthdate</label>
                </div>
                <div class="col-md-5">
                    <input type="date" class="form-control reg_birthdate required" id="birthdate">
                </div>
                <div class="col-md-1"  style="padding-top:8px;"></div>
                <!-- End of Birthdate -->

                <div class="col-md-12"><hr></div>

                <!-- Email -->
                <div class="col-md-2"></div>
                <div class="col-md-3" style="padding-top:5px;">
                    <label for="email"><small style="color:red;">* </small>Email</label>
                </div>
                <div class="col-md-5">
                    <input type="text" class="form-control reg_email required" id="email" onfocus="verifyEmail(this.id)">
                </div>
                <div class="col-md-1"  style="padding-top:8px;"></div>
                <!-- End of Email -->

                <div class="col-md-12 div_spacing-10"></div>

                <!-- Username -->
                <div class="col-md-2"></div>
                <div class="col-md-3" style="padding-top:5px;">
                    <label for="username"><small style="color:red;">* </small>Username</label>
                </div>
                <div class="col-md-5">
                    <input type="text" class="form-control reg_username required" id="username">
                </div>
                <div class="col-md-1"  style="padding-top:8px;"></div>
                <!-- End of Username -->

                <div class="col-md-12 div_spacing-10"></div>

                <!-- Password -->
                <div class="col-md-2"></div>
                <div class="col-md-3" style="padding-top:5px;">
                    <label for="password"><small style="color:red;">* </small>Password</label>
                </div>
                <div class="col-md-5">
                    <input type="password" class="form-control reg_password required" id="password" onfocus="verifyPassword(this.id)">
                </div>
                <div class="col-md-1"  style="padding-top:8px;"></div>
                <!-- End of Password -->

                <div class="col-md-12 div_spacing-10"></div>

                <!-- Confirm Password -->
                <div class="col-md-2"></div>
                <div class="col-md-3" style="padding-top:5px;">
                    <label for="confirmpass"><small style="color:red;">* </small>Confirm Password</label>
                </div>
                <div class="col-md-5">
                    <input type="password" class="form-control reg_confirmpass required" id="confirmpass" onfocus="validateConfirmPass(this.id)">
                </div>
                <div class="col-md-1"  style="padding-top:8px;"></div>
                <!-- End of Confirm Password -->

                <div class="col-md-12" style="padding-top:20px;"></div>

                <div class="col-md-12" style="text-align: center;">
                    <button class="btn regBtn" style="background-color: #db0c34; color: white;">Sign-up!</button>
                </div>
            </div>
        </div>
        </div>
    </body>

</html>

<script>
    $(".regBtn").on("click", function() {
        var null_counter = 0, error_counter = 0;
        $(".reg_required_warning").remove();

        $(".reg_div").find("input.required").each(function() {
            var input_val = $(this).val();
            if (input_val === null || input_val === "" || input_val === undefined) {
                null_counter++;
                $(this).closest("div").next("div").append("<small style='color:red;' class='reg_required_warning'>Required.</small>");
            }

            if ($(this).attr("id") === "email") {
                if ($(this).closest("div").find("small.reg_email_valid_warning").length == 1) {
                    error_counter++;
                }
            }

            if ($(this).attr("id") === "password") {
                if ($(this).closest("div").find("small.reg_password_valid_warning").length == 1) {
                    error_counter++;
                }
            }

        });

        if (null_counter === 0) {
            if (error_counter === 0) {
                var password = $('.reg_password').val(),
                confirm_pass = $('.reg_confirmpass').val();

                if (password === confirm_pass) {
                    var register_data = {
                        firstname:  $('.reg_firstname').val(),
                        middlename: $('.reg_middlename').val(),
                        lastname:   $('.reg_lastname').val(),
                        birthdate: $('.reg_birthdate').val(),
                        email: $('.reg_email').val(),
                        username: $('.reg_username').val(),
                        password: btoa(password)
                    };
                    saveSignUp(register_data);
                } else {      
                    Swal.fire({
                        title: "Password does not match.",
                        text: "",
                        icon: "warning",
                        confirmButtonColor: '#17a2b8'
                    });
                }
            } else {
                Swal.fire({
                    title: "Error in format.",
                    text: "Error in format on some fields has been found.",
                    icon: "warning",
                    confirmButtonColor: '#17a2b8'
                }); 
            }
            
        } else {
            Swal.fire({
                title: "Some field/s are empty!",
                text: "Please fill-up all required fields.",
                icon: "warning",
                confirmButtonColor: '#17a2b8'
            });
        }
    });

    $(".loginBtn").on("click", function() {
        var null_counter = 0, error_counter = 0;
        $(".login_required_warning").remove();

        $(".login_div").find("input.required").each(function() {
            var input_val = $(this).val();
            if (input_val === null || input_val === "" || input_val === undefined) {
                null_counter++;
                $(this).next("div").append("<small style='color:red; background-color:white;' class='login_required_warning'>Required.</small>");
            }
        });

        if (null_counter === 0){
            var username = $(".login_username").val(),
            password = $(".login_password").val();

            var login_data = {
                username : username,
                password : btoa(password)
            };

            loginFunc(login_data);
        }
    });

function verifyEmail(email_id) {
    $("#" + email_id).on("keyup", function() {
        var email = $(this).val();
        const res = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        
        $(".reg_email_valid_warning").remove();

        if (!res.test(String(email).toLowerCase()) && (email != null && email != "" && email != undefined)) {
            $(this).closest("div").append("<small style='color:red;' class='reg_email_valid_warning'>Invalid email address.</small>");
        }
    });
}

function verifyPassword(password_id) {
    $("#"+password_id).on("keyup", function() {
        var password = $(this).val();
        const pass_validator = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}$/;
        
        $(".reg_password_valid_warning").remove();

        if (!pass_validator.test(String(password))  && (password != null && password != "" && password != undefined)) {
            $(this).closest("div").append("<small style='color:red;' class='reg_password_valid_warning'>Password should be atleast 8-20 characters and contains at least one uppercase, lowercase and number.</small>");
        }
    });
}

function validateConfirmPass(confirmpass_id) {
    $("#"+confirmpass_id).on("keyup", function() {
        var password = $(".reg_password").val(),
        confirm_pass = $(this).val();

        $(".reg_confirmpass_valid_warning").remove();

        if (password != confirm_pass && (confirm_pass != null && confirm_pass != "" && confirm_pass != undefined)) {
            $(this).closest("div").append("<small style='color:red;' class='reg_confirmpass_valid_warning'>Password did not matched.</small>");
        }
    });
}

function saveSignUp(register_data) {
    $.ajax({
        type: "POST",
        dataType: "json", 
        data: {
                data : JSON.stringify(register_data)
        },
        url: "../ajax/register_new_user.php", 
        success: function(response) {
            if (response === 1) {
                Swal.fire({
                    title: "Successfully registered!",
                    text: "Please try to login.",
                    icon: "success",
                    confirmButtonColor: '#17a2b8'
                }).then((result) => {
                if (result.isConfirmed) {
                    location.reload();
                }
                });
            } else {
                Swal.fire({
                    title: "Something went wrong",
                    text: "",
                    icon: "error",
                    confirmButtonColor: '#17a2b8'
                });
            }
        }
    });
} 

function loginFunc(login_data) {
    $.ajax({
        type: "POST",
        dataType: "json", 
        data: {
                data : JSON.stringify(login_data)
        },
        url: "../ajax/login.php", 
        success: function(response) {
            if (response === 1) {
                var redirectPage = "6-1_dashboard.php";
                location.replace(redirectPage);
            } else if (response === 2) {
                Swal.fire({
                    title: "Invalid credentials.",
                    text: "Incorrect username and/or password.",
                    icon: "error",
                    confirmButtonColor: '#17a2b8'
                });
            } else {
                Swal.fire({
                    title: "Something went wrong",
                    text: "",
                    icon: "error",
                    confirmButtonColor: '#17a2b8'
                });
            }
            
        }
    });
}
</script>