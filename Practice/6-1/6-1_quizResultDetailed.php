<?php
    session_start();
    if (empty($_SESSION['username']) && empty($_SESSION['password'])) {
        header('Location: 6-1_login.php');
    }

    $basename_server = basename($_SERVER['SCRIPT_NAME']);
    
    if (empty($_GET['id'])) {
        echo '<p style="text-align:center;color:red;padding-top:50px;font-size:30px;">Page not found.</p>';
        exit;
    }
?>

<style>
    .highlighted-correct-answer{
        background-color: #b3ffb3;
    }
</style>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <title>6-1 Exercise</title>
        <!-- JQuery package -->
        <script src="../assets/jQuery/jquery.js"></script>
        <!-- swal package -->
        <script src="../assets/js/swal.js"></script>
        <!-- Packages -->
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
        <link href="../assets/css/styles.css" rel="stylesheet" />
        <link href="../assets/css/custom.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
    </head>
    <body class="sb-nav-fixed">
        <input type="hidden" class="resultId" value="<?= htmlspecialchars($_GET['id'])?>">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-yns">
            <!-- Navbar Brand-->
            <a class="navbar-brand ps-3" href="6-1_dashboard.php"><img src="../assets/img/YNS_logo.png" style="width: 20%;">&nbsp; Exercise 6-1</a>
            <!-- Sidebar Toggle-->
            <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!"><i class="fas fa-bars"></i></button>
            <!-- Navbar Search-->
            <div class="d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0">
            </div>
            <!-- Navbar-->
            <ul class="navbar-nav ms-auto ms-md-0 me-3 me-lg-4">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="6-1_logout.php">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-light" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav sidebar">
                            <!-- User Info -->
                            <div class="user-info row">
                                <div class="user-container">
                                    <img src="<?= $_SESSION['image'] ?>" width="48" height="48" alt="User" />
                                </div>
                                <div class="info-container div-col">
                                    <div class="info-header">Welcome,</div>
                                    <div class="name"><?= $_SESSION['username'] ?></div>
                                </div>
                            </div>
                            <!-- #User Info -->
                            <div class="sb-sidenav-menu-heading">Menu</div>
                            <a class="nav-link <?= $basename_server == '6-1_dashboard.php' || $basename_server == '6-1_quizResultDetailed.php' ? 'active' : ''?>" href="6-1_dashboard.php">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Dashboard
                            </a>
                            <a class="nav-link <?= $basename_server == '6-1_quiz.php' ? 'active' : ''?>" href="6-1_quiz.php">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Quiz
                            </a>        
                        </div>
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid px-4">
                        <h3 class="mt-4">Quiz - Multiple Choice</h3>
                        <div class="resultInfo">
                        </div>
                        <div style="padding-top:30px;"></div>
                        <div class="questions-div">

                        </div>
                    </div>
                </main>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="../assets/js/scripts.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
        <script src="../assets/js/datatables-simple-demo.js"></script>
    </body>
</html>
<script>
$(function() {
    var results_questionsOptions = getQuestionsOptions();
    var questionsArr = [], optionsArr = [], resultsArr = [];

    if (results_questionsOptions != null) {
        responseVal = parseInt(results_questionsOptions['response']),
        resultsArr = results_questionsOptions['resultsData'],
        questionsArr = results_questionsOptions['questionsArr'],
        optionsArr = results_questionsOptions['optionsArr'];
        if (responseVal === 1) {
            var divHtmlAppendStr = '';
            divHtmlAppendStr += '<b> Result ID: </b>' + 'Result #' + resultsArr[0]['results_id'].padStart(5, '0') + '<br/>'
                             +  '<b> Result: </b>' + (resultsArr[0]['results_result'] / resultsArr[0]['quiz_count']) + '/' + resultsArr[0]['quiz_count'] + '<br/>'
                             +  '<b> Equivalent Percentage: </b>' + resultsArr[0]['results_result'] + '%<br/>'
                             +  '<b> Remarks: </b>' + resultsArr[0]['results_remarks'] + '<br/>'
                             +  '<b> Date Taken: </b>' + resultsArr[0]['results_date_taken'] + '<br/>';
            $('.resultInfo').html(divHtmlAppendStr);
            
            var questionsNum = 1, htmlAppendStr = '';

            $(questionsArr).each(function() {
                var question = this['questions_description'], 
                    id = this['questions_id'],
                    answerId = this['options_id'];

                htmlAppendStr += '<p><b> Question #' + questionsNum + ': ' + question + '</b></p>'
                              +  '<div class="form-check required">';

                let questionOptions = optionsArr.filter(function(obj) {
                    return parseInt(obj.questions_id) === parseInt(id)
                });

                $(questionOptions).each(function() {
                    var option = this['options_description'], 
                        optionId = this['options_id'],
                        optionIsCorrect = this['options_is_correct_answer'],
                        correctAnswerFlag = 1;
                    var correctAnswerHighlight = parseInt(optionIsCorrect) === correctAnswerFlag ? 'highlighted-correct-answer' : '';
                    var optionAnswerMatched = parseInt(answerId) === parseInt(optionId) ? true : false,
                    checked = optionAnswerMatched ? 'checked' : '';
                    htmlAppendStr += '<input type="radio" class="form-check-input" id="ques' + questionsNum + '" value="' + optionId + '" disabled ' + checked + '>'
                                  +  '<label for="ques' + questionsNum + '" class="form-check-label ' + correctAnswerHighlight + '">' + option + '</label><br>';
                });

                htmlAppendStr += '</div>'
                              +  '<div style="padding-top:20px;"></div>';

                questionsNum++;
            });

            $(".questions-div").html(htmlAppendStr);
        } else if (responseVal === 2) {
            Swal.fire({
                title: "Session not found",
                text: "Redirecting to login page.",
                icon: "error",
                confirmButtonColor: '#17a2b8'
            }).then((result) => {
                if (result.isConfirmed) {
                    var redirectPage = "6-1_login.php";
                    location.replace(redirectPage);
                }
            });
        } else {
            Swal.fire({
                    title: 'Something went wrong',
                    text: 'Problem encountered in query.',
                    icon: 'error',
                    confirmButtonColor: '#17a2b8'
            });
        }
    } else {
        Swal.fire({
                title: 'Something went wrong',
                text: 'No data fetched.',
                icon: 'error',
                confirmButtonColor: '#17a2b8'
        });
    }

});  

function getQuestionsOptions() {
    var responseData = [];
    $.ajax({
        type: 'POST',
        dataType: 'json', 
        data: {
            id : $('input.resultId').val()
        },
        url: '../ajax/quiz_results_detailed.php', 
        async: false,
        success: function(response) {
            responseData = response;
        }
    });

    return responseData;
}
</script>