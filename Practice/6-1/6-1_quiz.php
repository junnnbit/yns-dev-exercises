<?php
    session_start();
    if (empty($_SESSION['username']) && empty($_SESSION['password'])) {
        header('Location: 6-1_login.php');
    }

    $basename_server = basename($_SERVER['SCRIPT_NAME']);
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <title>6-1 Exercise</title>
        <!-- JQuery package -->
        <script src="../assets/jQuery/jquery.js"></script>
        <!-- waitMe -->
        <script src="../assets/waitMe/waitMe.js"></script>
        <link href="../assets/waitMe/waitMe.css" rel="stylesheet" />
        <!-- swal package -->
        <script src="../assets/js/swal.js"></script>
        <!-- Packages -->
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@latest/dist/style.css" rel="stylesheet" />
        <link href="../assets/css/styles.css" rel="stylesheet" />
        <link href="../assets/css/custom.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-yns">
            <!-- Navbar Brand-->
            <a class="navbar-brand ps-3" href="6-1_dashboard.php"><img src="../assets/img/YNS_logo.png" style="width: 20%;">&nbsp; Exercise 6-1</a>
            <!-- Sidebar Toggle-->
            <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!"><i class="fas fa-bars"></i></button>
            <!-- Navbar Search-->
            <div class="d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0">
            </div>
            <!-- Navbar-->
            <ul class="navbar-nav ms-auto ms-md-0 me-3 me-lg-4">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="6-1_logout.php">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-light" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav sidebar">
                            <!-- User Info -->
                            <div class="user-info row">
                                <div class="user-container">
                                    <img src="<?= $_SESSION['image'] ?>" width="48" height="48" alt="User" />
                                </div>
                                <div class="info-container div-col">
                                    <div class="info-header">Welcome,</div>
                                    <div class="name"><?= $_SESSION['username'] ?></div>
                                </div>
                            </div>
                            <!-- #User Info -->
                            <div class="sb-sidenav-menu-heading">Menu</div>
                            <a class="nav-link <?= $basename_server == '6-1_dashboard.php' ? 'active' : ''?>" href="6-1_dashboard.php">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Dashboard
                            </a>
                            <a class="nav-link <?= $basename_server == '6-1_quiz.php' ? 'active' : ''?>" href="6-1_quiz.php">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Quiz
                            </a>        
                        </div>
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid px-4">
                        <h3 class="mt-4">Quiz - Multiple Choice</h3>
                        <ol class="breadcrumb mb-4">
                            <li class="breadcrumb-item active">Select the best answer for every questions. Good luck!</li>
                        </ol>
                        <div style="padding-top:20px;"></div>
                        <div class="questions-div">

                        </div>
                        <div style="text-align: center; padding-top:60px; padding-bottom:60px;">
                            <button class="btn submitQuizBtn" style="background-color: #db0c34; color: white; width: 100%;" onClick="submitAnswer()">Submit!</button>
                        </div>
                    </div>
                </main>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="../assets/js/scripts.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
        <script src="../assets/js/datatables-simple-demo.js"></script>
    </body>
</html>
<script>
var answersArr = [];
$(function() {
    var questionsOptions = getQuestionsOptions();
    var questionsArr = [], optionsArr = [];

    if (questionsOptions != null) {
        responseVal = parseInt(questionsOptions['response']),
        questionsArr = questionsOptions['questionsArr'],
        optionsArr = questionsOptions['optionsArr'];
        if (responseVal === 1) {
            var questionsNum = 1, htmlAppendStr = '';

            $(questionsArr).each(function() {
                var question = this['questions_description'], 
                    id = this['questions_id'],
                    answerId = '', 
                    answerCorrect = '';
                
                answersArr.push({
                                    question_id: id,
                                    answer_id : answerId,
                                    answer_is_correct : answerCorrect
                                });

                htmlAppendStr += '<p><b> Question #' + questionsNum + ': ' + question + '</b></p>'
                              +  '<div class="form-check required">';

                let questionOptions = optionsArr.filter(function(obj) {
                    return parseInt(obj.questions_id) === parseInt(id)
                });

                $(questionOptions).each(function() {
                    var option = this['options_description'], 
                        optionId = this['options_id'],
                        optionIsCorrect = this['options_is_correct_answer'];
                    htmlAppendStr += '<input type="radio" class="form-check-input" id="ques' + questionsNum + '" name="question' + questionsNum + '" value="' + optionId + '" onChange="answerFunc(this, ' + id + ', ' + optionIsCorrect + ')">'
                                  +  '<label for="ques' + questionsNum + '" class="form-check-label">' + option + '</label><br>';
                });

                htmlAppendStr += '</div>'
                              +  '<div style="padding-top:20px;"></div>';

                questionsNum++;
            });

            $(".questions-div").html(htmlAppendStr);
        } else if (responseVal === 2) {
            Swal.fire({
                title: "Session not found",
                text: "Redirecting to login page.",
                icon: "error",
                confirmButtonColor: '#17a2b8'
            }).then((result) => {
                if (result.isConfirmed) {
                    var redirectPage = "6-1_login.php";
                    location.replace(redirectPage);
                }
            });
        } else {
            Swal.fire({
                    title: 'Something went wrong',
                    text: 'Problem encountered in query.',
                    icon: 'error',
                    confirmButtonColor: '#17a2b8'
            });
        }
    } else {
        Swal.fire({
                title: 'Something went wrong',
                text: 'No data fetched.',
                icon: 'error',
                confirmButtonColor: '#17a2b8'
        });
    }

});  

function getQuestionsOptions() {
    var responseData = [];
    $.ajax({
        type: 'POST',
        dataType: 'json', 
        url: '../ajax/quizzes.php', 
        async: false,
        success: function(response) {
            responseData = response;
        }
    });

    return responseData;
}

function answerFunc(element, questionId, isAnswer) {
    var answersArrIndex = answersArr.findIndex( obj => parseInt(obj.question_id) === parseInt(questionId) );

    answersArr[answersArrIndex]['answer_id'] = $(element).val();
    answersArr[answersArrIndex]['answer_is_correct'] = isAnswer;
}

function submitAnswer(){
    $('body').waitMe();

    var null_counter = 0;
    $('.quiz_answer_req').remove();

    $('.questions-div').each(function() {
        var questions_div = this;
        var required_div_index = 0;
        $(questions_div).find('div.required').each(function() {
            var radio_name = $(this).find('input').attr('name');
            var radio_value = $('input[name="' + radio_name + '"]:checked').val();
            if (radio_value == null) {
                null_counter++;
                $(questions_div).find('p').eq(required_div_index).append('<small style="color: red; padding-left: 10px;" class="quiz_answer_req">Required to answer.</small>');
            }
            required_div_index++;
        });
    });

    if (null_counter !== 0) {
        Swal.fire({
                title: 'No answer found on some questions.',
                text: 'Please answer all questions.',
                icon: 'error',
                confirmButtonColor: '#17a2b8'
        });
        $('body').waitMe('hide');
    } else {
        resultComputationAndSave();
    }
}

function resultComputationAndSave() {
    var correctAnswerCount = 0, 
        correctAnswerFlag = 1,
        numberOfQuestions = answersArr.length;

    $(answersArr).each(function() {
        var answerRow = this;
        if (parseInt(answerRow['answer_is_correct']) === correctAnswerFlag) {
            correctAnswerCount++;
        }
    });

    var resultPercentage = (correctAnswerCount / numberOfQuestions) * 100;
    var passingGrade = 70,
        resultRemarks = '';

    if (resultPercentage >= passingGrade) {
        resultRemarks = 'PASSED';
    } else {
        resultRemarks = 'FAILED';
    }

    var resultArr = {
        result_percentage : resultPercentage,
        result_remarks : resultRemarks
    };

    var quizData = {
        answers_array : answersArr,
        result_array : resultArr
    };

    $.ajax({
        type: "POST",
        dataType: "json", 
        data: {
                data : JSON.stringify(quizData)
        },
        url: "../ajax/insert_quiz_result.php", 
        success: function(response) {
            $('body').waitMe('hide');
            if (response === 1) {
                Swal.fire({
                    title: "Successfully registered!",
                    text: "Please try to login.",
                    icon: "success",
                    confirmButtonColor: '#17a2b8'
                }).then((result) => {
                    if (result.isConfirmed) {
                        location.reload();
                    }
                });
                Swal.fire({
                    title: '<h4>YOUR RESULT IS</h4>',
                    html:
                        '<h1>' + resultPercentage + '% </h1>'  +
                        '<h4>' + resultRemarks + '! </h4>' +
                        '<div style="padding-top:20px;"></div>' +
                        '<h6> You have answered ' + correctAnswerCount + '/' + numberOfQuestions + ' questions correctly.</h6>',
                    confirmButtonColor: '#17a2b8'
                }).then((result) => {
                    if (result.isConfirmed) {
                        location.reload();
                    }
                });
            } else if (response === 2) {
                Swal.fire({
                    title: "Session not found",
                    text: "Redirecting to login page.",
                    icon: "error",
                    confirmButtonColor: '#17a2b8'
                }).then((result) => {
                    if (result.isConfirmed) {
                        var redirectPage = "6-1_login.php";
                        location.replace(redirectPage);
                    }
                });
            } else {
                Swal.fire({
                    title: "Something went wrong",
                    text: "",
                    icon: "error",
                    confirmButtonColor: '#17a2b8'
                });
            }
        }
    });
}
</script>