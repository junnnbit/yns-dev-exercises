<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <title>6-3 Exercise</title>
        <!-- JQuery package -->
        <script src="assets/jQuery/jquery.js"></script>
        <!-- Bootstrap -->
        <link href="assets/bootstrap/bootstrap.min.css" rel="stylesheet">
        <script src="assets/bootstrap/bootstrap.min.js"></script>
        <!-- Icons -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
    </head>
    <body>
        <div class="container">
            <div class="col-md-12" style="padding-top: 50px;">
                <a href="6-1/6-1_login.php" title="This will redirect you to 6-1/6-1_login.php" class="btn btn-info btn-lg" style="width: 100%;" role="button">Go to Quiz</a>
                <div style="padding-top: 20px;"></div>
                <a href="6-2.php" title="This will redirect you to 6-2.php" class="btn btn-info btn-lg" style="width: 100%;" role="button">Go to Calendar</a>
            </div>
        </div>
    </body>
</html>