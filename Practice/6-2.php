<style>
    .calendar > thead > tr > td {
        width: 14.285714%;
        text-align: center;
    }

    .calendar > tbody > tr > td {
        text-align: center;
        height: 80px;
    }

    .highlight-not-in-month {
        background-color: #dedede;
        color: #000000;
    }

    .present-date {
        background-color: #262bf0;
        color: #ffffff;
    }
</style>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <title>6-2 Exercise</title>
        <!-- JQuery package -->
        <script src="assets/jQuery/jquery.js"></script>
        <!-- Bootstrap -->
        <link href="assets/bootstrap/bootstrap.min.css" rel="stylesheet">
        <script src="assets/bootstrap/bootstrap.min.js"></script>
        <!-- Icons -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
    </head>
    <body>
        <div class="container">
            <div class="row" style="padding-top:20px;padding-bottom: 20px;">
                <div class="col-md-2"></div>
                <div class="col-md-2" style="text-align:right;"><button class="btn bg-transparent" onClick="prevMonth()"><h1><i class="fa fa-caret-left"></i></h1></button></div>
                <div class="col-md-4" style="text-align:center;"><h1 class='monthTitle'>SEPTEMBER 2021</h1></div>
                <div class="col-md-2"><button class="btn bg-transparent" onClick="nextMonth()"><h1><i class="fa fa-caret-right"></i></h1></button></div>
                <div class="col-md-2"></div>
            </div>
            <table class="table table-bordered calendar" style="width:100%;">
                <thead>
                    <tr>
                        <td>Sunday</td>
                        <td>Monday</td>
                        <td>Tuesday</td>
                        <td>Wednesday</td>
                        <td>Thursday</td>
                        <td>Friday</td>
                        <td>Saturday</td>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </body>
</html>
<script>
var date = new Date();
var months = [
                'January', 
                'February',
                'March',
                'April',
                'May',
                'June',
                'July',
                'August',
                'September',
                'October',
                'November',
                'December'
             ];
var currentMonth = date.getMonth() + 1;
var currentYear = date.getFullYear();
var activeMonth = date.getMonth() + 1;
var activeYear = date.getFullYear();

buildCalendar();

function dayInMonth (month, year, day) {
    return new Date(year, month, day).getDate();
}

function prevMonth(){
    activeMonth = activeMonth - 1;
    if (activeMonth === 0) {
        activeMonth = 12;
        activeYear = activeYear - 1;
    } 
    buildCalendar();
}

function nextMonth(){
    activeMonth = activeMonth + 1;
    if (activeMonth > 12) {
        activeMonth = 1;
        activeYear = activeYear + 1;
    } 
    buildCalendar();
}

function getCalendarArray() {
    var firstDayOfMonth = dayInMonth(activeMonth, activeYear, 1);
    var lastDayOfMonth = dayInMonth(activeMonth, activeYear, 0);
    var firstDateOfMonth = activeMonth + '/' + firstDayOfMonth + '/' + activeYear;
    var monthFirstDayOfWeek = new Date(firstDateOfMonth).getDay();
    var daysInWeek = 7;

    var calendarArr = [];
    var maxRow = 6;

    var monthDay = firstDayOfMonth;
    var lastDate = activeMonth + '/' + lastDayOfMonth + '/' + activeYear;

    for (let rowCount = 0; rowCount < maxRow; rowCount++) {
        var daysInWeekArr = [];
        for (let daysInWeekArrIndex = 0; daysInWeekArrIndex < daysInWeek; daysInWeekArrIndex++) {
            if (monthDay === firstDayOfMonth && daysInWeekArrIndex !== monthFirstDayOfWeek) {
                for (let x = monthFirstDayOfWeek; x > 0; x--) {
                    var prevDay = new Date(new Date(firstDateOfMonth).setDate(new Date(firstDateOfMonth).getDate()-x)).getDate();
                    daysInWeekArr[daysInWeekArrIndex] = {day: prevDay, is_current: false, not_in_month: true};
                    daysInWeekArrIndex++;
                }
                daysInWeekArrIndex--;
            } else {
                if (monthDay > lastDayOfMonth) {
                    var extraDays = daysInWeek - (daysInWeekArrIndex);
                    for (let y = 1; y <= extraDays; y++) {
                        var nextDate = new Date(new Date(lastDate).setDate(new Date(lastDate).getDate()+y));
                        daysInWeekArr[daysInWeekArrIndex] = {day: nextDate.getDate(), is_current: false, not_in_month: true};
                        daysInWeekArrIndex++;
                    }
                    lastDate = (nextDate.getMonth() + 1) + '/' + nextDate.getDate() + '/' + nextDate.getFullYear();
                } else {
                    if (monthDay === date.getDate() && currentMonth === activeMonth) {
                        daysInWeekArr[daysInWeekArrIndex] = {day: monthDay, is_current: true, not_in_month: false};
                    } else {
                        daysInWeekArr[daysInWeekArrIndex] = {day: monthDay, is_current: false, not_in_month: false};
                    }
                }
                monthDay++;
            }
        }
        calendarArr.push(daysInWeekArr);
    }

    return calendarArr;
}

function buildCalendar() {
    var calendarArr = getCalendarArray();
    var htmlAppend = '';
    
    $(calendarArr).each(function() {
        weekRow = this;
        htmlAppend += '<tr>';

        $(weekRow).each(function() {
            dayRow = this;
            highlight_not_in_month = this['not_in_month'] ? 'highlight-not-in-month' : '';
            highlight_present_date = this['is_current'] ? 'present-date' : '';
            htmlAppend += '<td class="' + highlight_not_in_month + ' ' + highlight_present_date + '">' + this['day'] + '</td>';
        });

        htmlAppend += '</tr>';
    });

    activeMonthYear = months[activeMonth-1] + ' ' + activeYear;

    $('.monthTitle').html(activeMonthYear);
    $('table.calendar>tbody').html(htmlAppend);
}
</script>