<?php
    $noSessionFoundFlag = 2;
    $errorFlag = 0;
    $successFlag = 1;

    session_start();
    if (empty($_SESSION['username']) && empty($_SESSION['password'])) {
        $response = $noSessionFoundFlag;
    } else {
        include 'db_conn.php';

        $currentUserId = $_SESSION['id'];

        $data = json_decode($_POST['data'], true);
        $answersArr = $data['answers_array'];
        $resultArr = $data['result_array'];

        // prepare and bind - results
        $resultStmt = $conn->prepare('INSERT INTO results_tbl(users_id, results_result, results_remarks) VALUES(?, ?, ?)');
        $resultStmt->bind_param('iis', $currentUserId, $resultPercentage, $resultRemarks);

        // set parameters and execute - results
        $resultPercentage = $resultArr['result_percentage'];
        $resultRemarks = $resultArr['result_remarks'];
        
        if ($resultStmt->execute()) {
            $resultsId = $conn->insert_id;

            $dataToInsertCount = count($answersArr);
            $dataInsertedCount = 0;

            // prepare and bind - results history
            $resultHistStmt = $conn->prepare('INSERT INTO results_hist_tbl(results_id, users_id, questions_id, options_id) VALUES(?, ?, ?, ?)');
            $resultHistStmt->bind_param('iiii', $resultsId, $currentUserId, $questionsId, $optionsId);

            foreach ($answersArr as $ans) {
                // set parameters and execute - results history
                $questionsId = intval($ans['question_id']);
                $optionsId = intval($ans['answer_id']);

                if ($resultHistStmt->execute()) {
                    $dataInsertedCount++;
                }
            }

            if ($dataInsertedCount === $dataToInsertCount) {
                $response = $successFlag;
            } else {
                $response = $errorFlag;
            }
        } else {
            $response = $errorFlag;
        }
    }
    
    echo $response;
    
?>