<?php
    $noSessionFoundFlag = 2;
    $errorFlag = 0;
    $successFlag = 1;

    $returnedData = [];
    $responseArr = [];

    session_start();
    if (empty($_SESSION['username']) && empty($_SESSION['password'])) {
        $response = $noSessionFoundFlag;
        $responseArr = ['response' => $response, 'data' => $returnedData];
    } else {
        include 'db_conn.php';
        $currentUserId = $_SESSION['id'];

        $sql = <<<QUERY
        SELECT res.*, COUNT(*) AS quiz_count 
        FROM results_tbl AS res 
        INNER JOIN results_hist_tbl AS res_hist 
        ON res.results_id = res_hist.results_id 
        WHERE res.users_id = {$currentUserId}
        GROUP BY results_id
        QUERY;
        $result = $conn->query($sql);

        if (!$result) {
            $response = $errorFlag;
        } else {
            $response = $successFlag;
            while ($row = mysqli_fetch_array($result)) {
                $row['results_date_taken'] = date('F d, Y H:i:s A', strtotime($row['results_date_taken']));
                array_push($returnedData, $row);
            }
        }

        $responseArr = ['response' => $response, 'data' => $returnedData];
    }

    echo json_encode($responseArr);


?>