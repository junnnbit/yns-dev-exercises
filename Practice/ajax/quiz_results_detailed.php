<?php
    $noSessionFoundFlag = 2;
    $errorFlag = 0;
    $successFlag = 1;

    session_start();
    if (empty($_SESSION['username']) && empty($_SESSION['password'])) {
        $response = $noSessionFoundFlag;
        $responseArr = ['response' => $response, 'data' => $returnedData];
    } else {
        include 'db_conn.php';
        $currentUserId = $_SESSION['id'];
        $resultsId = $_POST['id'];

        $resultsData = [];
        $responseArr = [];

        $questionsArr = [];
        $questionsId = [];
        $optionsArr = [];

        $sql = <<<QUERY
        SELECT res.*, COUNT(*) AS quiz_count 
        FROM results_tbl AS res 
        INNER JOIN results_hist_tbl AS res_hist 
        ON res.results_id = res_hist.results_id 
        WHERE res.users_id = {$currentUserId}
        AND res.results_id = {$resultsId}
        GROUP BY results_id
        QUERY;
        $result = $conn->query($sql);

        if (!$result) {
            $response = $errorFlag;
        } else {
            while ($row = mysqli_fetch_array($result)) {
                $row['results_date_taken'] = date('F d, Y H:i:s A', strtotime($row['results_date_taken']));
                array_push($resultsData, $row);
            }
        }

        if (!empty($resultsData)) {
            $questionsHistSql = <<<QUERY
            SELECT 
                result_hist.*, 
                questions.questions_description 
            FROM 
                results_hist_tbl AS result_hist 
            INNER JOIN 
                questions_tbl AS questions 
            ON 
                result_hist.questions_id = questions.questions_id 
            WHERE 
                result_hist.results_id = {$resultsId}
                AND result_hist.users_id = {$currentUserId}
            QUERY;
            $questionsHistResult = $conn->query($questionsHistSql);

            if (!$questionsHistResult) {
                $response = $errorFlag;
            } else {
                while ($questionsHistRow = mysqli_fetch_array($questionsHistResult)) {
                    array_push($questionsId, $questionsHistRow['questions_id']);
                    array_push($questionsArr, $questionsHistRow);
                }
            }
        }

        if (!empty($questionsArr) && !empty($questionsId)) {
            $optionsSql = 'SELECT * FROM options_tbl WHERE questions_id IN (' . implode(', ', $questionsId) . ')';
            $optionsResult = mysqli_query($conn, $optionsSql);

            if(!$optionsResult){
                $response = $errorFlag;
                $responseArr = ['response'=>$response, 'questionsArr'=>$questionsArr, 'optionsArr'=>$optionsArr];
                echo json_encode($responseArr);
                exit;
            } else {
                while ($optionsRow = mysqli_fetch_array($optionsResult)) {
                    $optionsRow['options_description'] = htmlspecialchars($optionsRow['options_description']);
                    array_push($optionsArr, $optionsRow);
                }
            }
        }

        if (!empty($resultsData) && !empty($questionsArr) && !empty($optionsArr)) {
            $response = $successFlag;
        } else {
            $response = $errorFlag;
        }

        $conn->close();
        $responseArr = ['response' => $response, 'resultsData' => $resultsData, 'questionsArr' => $questionsArr, 'optionsArr' => $optionsArr];
    }
    echo json_encode($responseArr);


?>