<?php
    include 'db_conn.php';
    $data = json_decode($_POST['data'], true);

    $username = $data['username'];
    $password = base64_encode($data['password']);
    $userStatus = 'ACTIVE';

    $sql = <<<QUERY
      SELECT * FROM users_tbl WHERE users_username = '{$username}' AND users_password = '{$password}' AND users_status = '{$userStatus}'
      QUERY;
    $result = $conn->query($sql);

    if (!$result) {
        $response = 0;
    } else {
        $row = $result->fetch_assoc();
        
        if (!empty($row)) {
            session_start();
            $_SESSION['id'] = $row['users_id'];
            $_SESSION['firstname'] = $row['users_firstname'];
            $_SESSION['middlename'] = $row['users_midname'];
            $_SESSION['lastname'] = $row['users_lastname'];
            $_SESSION['birthdate'] = $row['users_birthdate'];
            $_SESSION['address'] = $row['users_address'];
            $_SESSION['email'] = $row['users_email'];
            $_SESSION['username'] = $row['users_username'];
            $_SESSION['password'] = $row['users_password'];
            $_SESSION['image'] = "../".$row['users_image'];
            $_SESSION['status'] = $row['users_status'];

            $response = 1;
        } else {
            $response = 2;
        }
    }

    echo $response;


?>