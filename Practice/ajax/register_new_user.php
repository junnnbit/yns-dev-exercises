<?php
    include 'db_conn.php';
    $data = json_decode($_POST['data'], true);

    $firstName = $data['firstname'];
    $middleName = $data['middlename'];
    $lastName = $data['lastname'];
    $birthDate = $data['birthdate'];
    $email = $data['email'];
    $username = $data['username'];
    $password = base64_encode($data['password']);
    $image = 'assets/img/person-icon.png';
    $defaultStatus = 'ACTIVE';

    $sql = <<<QUERY
      INSERT INTO users_tbl(users_firstname, users_middlename, users_lastname, users_birthdate, users_email, users_username, users_password, users_image, users_status)       
      VALUES('{$firstName}', '{$middleName}', '{$lastName}', '{$birthDate}', '{$email}', '{$username}', '{$password}', '{$image}', '{$defaultStatus}')
      QUERY;

    if (mysqli_query($conn, $sql)) {
        $response = 1;
    } else {
        $response = 0;
    }

    echo $response;


    
?>