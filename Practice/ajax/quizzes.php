<?php
    $noSessionFoundFlag = 2;
    $errorFlag = 0;
    $successFlag = 1;

    session_start();
    if (empty($_SESSION['username']) && empty($_SESSION['password'])) {
        $response = $noSessionFoundFlag;
        $responseArr = ['response'=>$response];
    } else {
        include 'db_conn.php';

        $questionsArr = [];
        $questionsId = [];
        $optionsArr = [];
        $response = $errorFlag;

        $questionsSql = 'SELECT * FROM questions_tbl ORDER BY RAND() LIMIT 10';
        $questionsResult = $conn->query($questionsSql);

        if (!$questionsResult) {    
            $response = $errorFlag;
        } else {
            while ($questionsRow = $questionsResult->fetch_assoc()) {
                array_push($questionsArr, $questionsRow);
                array_push($questionsId, $questionsRow['questions_id']);
            }
        }

        if (!empty($questionsArr) && !empty($questionsId)) {
            $optionsSql = 'SELECT * FROM options_tbl WHERE questions_id IN (' . implode(', ', $questionsId) . ')';
            $optionsResult = mysqli_query($conn, $optionsSql);

            if (!$optionsResult) {
                $response = $errorFlag;
                $responseArr = ['response'=>$response, 'questionsArr'=>$questionsArr, 'optionsArr'=>$optionsArr];
                echo json_encode($responseArr);
                exit;
            } else {
                while ($optionsRow = mysqli_fetch_array($optionsResult)) {
                    $optionsRow['options_description'] = htmlspecialchars($optionsRow['options_description']);
                    array_push($optionsArr, $optionsRow);
                }
            }
        }

        if (!empty($questionsArr) && !empty($optionsArr)) {
            $response = $successFlag;
        } else {
            $response = $errorFlag;
        }

        $conn->close();
        $responseArr = ['response' => $response, 'questionsArr' => $questionsArr, 'optionsArr' => $optionsArr];
    }
    echo json_encode($responseArr);
?>