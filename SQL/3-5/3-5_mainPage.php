<?php
    session_start();
    if(empty($_SESSION['username']) && empty($_SESSION['password'])){
        echo "<p style='text-align:center;color:red;padding-top:50px;font-size:30px;'>Session not found. You cannot access this site.</p>";
        exit;
    }

    $conn = mysqli_connect("localhost", "root", "", "training_db") or die ("Database Error!"); 
    $sql = "SELECT * FROM user_info_tbl";
    $result = mysqli_query($conn, $sql);
    if($result == false){
        echo "<p style='text-align:center;color:red;'>Error in query.</p>";
        exit;
    }
    else{
        $counter = 0;
        $i = 0;
        $fetched_data = array();
        $array_main = array();
        $array_sub = array();
        while($row = mysqli_fetch_array($result)){
            array_push($fetched_data, $row);
        }
        $row_count = count($fetched_data);

        foreach($fetched_data as $r){
            if($i != ($row_count-1)){
                if($counter < 9){
                    array_push($array_sub, $r);
                    $counter++;
                }
                else{
                    array_push($array_sub, $r);
                    array_push($array_main, $array_sub);
                    $array_sub = array();
                    $counter = 0;
                }
            }
            else{
                if($counter < 9){
                    array_push($array_sub, $r);
                    array_push($array_main, $array_sub);
                }
                else{
                    array_push($array_sub, $r);
                    array_push($array_main, $array_sub);
                    $counter = 0;
                }
            }
            $i++;
        }

        
        $page = !empty($_GET['page']) ? (int) $_GET['page'] : 1;
        $data = !empty($array_main) ? $array_main[$page-1] : array();
        
    }
    mysqli_close($conn);


?>

<style>
    /* Table */
    table{
        width: 100%;
    }

    table, th, td{
        border: 1px solid black;
    }

    /* Pagination links */
    .pagination {
        padding-top: 20px;
    }
    .pagination a {
        color: black;
        float: left;
        padding: 8px 16px;
        text-decoration: none;
        transition: background-color .3s;
    }

    /* Style the active/current link */
    .pagination a.active {
        background-color: dodgerblue;
        color: white;
    }

    /* Add a grey background color on mouse-over */
    .pagination a:hover:not(.active) {background-color: #ddd;}

    /* Buttons cursor */
    button{
        cursor: pointer;
    }
</style>

<html>
    <head></head>

    <body>
        <div style="padding-top:20px; padding-bottom:20px;text-align:right;">
            <div style="font-size:17px;">
                <img src="<?php echo $_SESSION['image']?>" style="width: 50px; height: 50px; border: 1px solid black;">
                <div style="padding-top:10px;"></div>
                Welcome, <?php echo $_SESSION['username'];?> (<a style="color:blue; cursor:pointer;" href="3-5_logout.php"><u>logout</u></a>)
            </div>
        </div>
        <hr>
        <h2 style="text-align: center; padding-top: 20px;">Users List</h2>
        <button style="float:right;padding:10px;background-color: #4CAF50;" onClick="addBtn()">Add New User</button>
        <div style="padding-top:40px;padding-bottom:40px;"></div>
        <table class="table">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Middle Name</th>
                    <th>Last Name</th>
                    <th>Birtdate</th>
                    <th>Age</th>
                    <th>Address</th>
                    <th>Email</th>
                    <th>Username</th>
                    <th>Password</th>
                    <th>Image</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if(!empty($data)){
                        foreach($data as $d){
                ?>
                            <tr>
                                <td><?php echo $d['uit_firstname'];?></td>
                                <td><?php echo $d['uit_midname'];?></td>
                                <td><?php echo $d['uit_lastname'];?></td>
                                <td><?php echo $d['uit_birthdate'];?></td>
                                <td><?php echo $d['uit_age'];?></td>
                                <td><?php echo $d['uit_address'];?></td>
                                <td><?php echo $d['uit_email'];?></td>
                                <td><?php echo $d['uit_username'];?></td>
                                <td><?php echo $d['uit_password'];?></td>
                                <td style="text-align:center;"><img src="<?php echo "../".$d['uit_image'];?>" style="height: 50px; width: 50px;"></td>
                                <td><button name="openBtn" onClick="redirect(<?php echo $d['uit_id'];?>)">Open Info</button></td>
                            </tr>
                <?php
                        }
                    }
                    else{
                ?>
                        <tr>
                            <td colspan='11' style="text-align: center; color: red;">No data found.</td>
                        </tr>
                <?php
                    }
                ?>
            </tbody>
        </table>
        <div class="pagination">
            <div style="float:left;">
                <?php echo "Page ".$page." of ".(!empty($array_main) ? count($array_main) : 1);?>
            </div>
            <div style="float:right;">
                <?php
                    if($page == 1){
                ?>
                        <a href="#">&laquo;</a>
                <?php    
                    }
                    else{
                ?>
                        <a href="<?php echo "3-5_mainPage.php?page=1";?>">&laquo;</a>
                <?php
                    }
                ?>
                <?php 
                    for($i=1; $i<(count($array_main)+1); $i++){
                        if($i == $page){
                ?>
                            <a class="active" href="#"><?php echo $i?></a>
                <?php
                        }
                        else{
                ?>
                            <a href="<?php echo "3-5_mainPage.php?page=".$i;?>"><?php echo $i?></a>
                <?php
                        }
                    }
                ?>
                <?php
                    if($page == count($array_main)){
                ?>
                        <a href="#">&raquo;</a>
                <?php    
                    }
                    else{
                ?>
                        <a href="<?php echo "3-5_mainPage.php?page=".count($array_main);?>">&raquo;</a>
                <?php
                    }
                ?>
            </div>
        </div>
    </body>
</html>

<script>
if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
}

// var button = document.getElementByName("openBtn");
// button.addEventListener('click', function() {
//   document.location.href = '1-10_form.php';
// });
function redirect(index){
    document.location.href = '3-5_imagePage.php?index='+index;
}

function addBtn(){
    document.location.href = '3-5_addPage.php';
}
</script>
