<?php
    session_start();
    if(empty($_SESSION['username']) && empty($_SESSION['password'])){
        echo "<p style='text-align:center;color:red;padding-top:50px;font-size:30px;'>Session not found. You cannot access this site.</p>";
        exit;
    }
    $index = $_GET['index'];
    $data = [];

    $conn = mysqli_connect("localhost", "root", "", "training_db") or die ("Database Error!"); 
    $sql = "SELECT * FROM user_info_tbl WHERE uit_id = ".$index;
    $result = mysqli_query($conn, $sql);
    if($result == false){
        echo "<p style='text-align:center;color:red;'>Error in query.</p>";
        exit;
    }
    else{
        $data = mysqli_fetch_array($result);
    }
    mysqli_close($conn);
?>

<html>
    <head></head>

    <body>
    <?php
        if(!empty($data)){
    ?>
        <form method="post" enctype="multipart/form-data">
            <div style="padding-left:50px;">
                <h3>User Basic Information:</h3>
            </div>
            <div style="padding-right:500px;">
                <img src="<?php echo "../".$data['uit_image'];?>" style="float:right;width:200px;height:200px;border:1px solid black;">
            </div>
            <div style="padding-left:50px;">
                <label for="inputFirstNameID"> First Name: </label>
                <input type="text" id="inputFirstNameID" value="<?php echo $data['uit_firstname'];?>" readonly>
                <br/><br/>
                <label for="inputMidNameID"> Middle Name: </label>
                <input type="text" id="inputMidNameID" value="<?php echo $data['uit_midname'];?>" readonly>
                <br/><br/>
                <label for="inputLastNameID"> Last Name: </label>
                <input type="text" id="inputLastNameID" value="<?php echo $data['uit_lastname'];?>" readonly>
                <br/><br/>
                <label for="inputBirthdateID"> Birthdate: </label>
                <input type="date" id="inputBirthdateID" value="<?php echo $data['uit_birthdate'];?>" readonly>
                <br/><br/>
                <label for="inputAgeID"> Age: </label>
                <input type="text" id="inputAgeID" value="<?php echo $data['uit_age'];?>" readonly>
                <br/><br/>
                <label for="inputAddressID"> Address: </label>
                <input type="text" id="inputAddressID" value="<?php echo $data['uit_address'];?>" readonly>
                <br/><br/>
            </div>
            <hr>
            <div style="padding-left:50px;">
                <br/><br/>
                <label for="inputEmailID"> Email: </label>
                <input type="text" id="inputEmailID" value="<?php echo $data['uit_email'];?>" readonly>
                <br/><br/>
                <label for="inputUserNameID"> Username: </label>
                <input type="text" id="inputUserNameID" value="<?php echo $data['uit_username'];?>" readonly>
                <br/><br/>
                <label for="inputUserPassID"> Password: </label>
                <input type="password" id="inputUserPassID" value="<?php echo $data['uit_password'];?>" readonly>
                <br/><br/><br/>
                <label for="inputImageID"> Upload Image: </label>
                <input type="file" id="inputImageID" name="inputImage" accept="image/*">
                <br/><br/><br/>
                <button type="submit" name="submitButton">Submit</button>
            </div>
        </form>
    <?php
        }
        else{
    ?>
        <h2 style="text-align: center;">No data found.</h2>
    <?php
        }
    ?>
    </body>
</html>

<script>
if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
}
</script>

<?php
    if(isset($_POST['submitButton'])){
        $error = 0;
        $root_filepath = $_SERVER['DOCUMENT_ROOT']."/yns-dev-exercises/SQL/";
        $file_folder = "assets";
        $filename = $_FILES["inputImage"]["name"];
        $newFilename = $file_folder."/"."(".date('mdYHis').")".basename($filename);
        $file = $root_filepath.$newFilename;
        $fileType = strtolower(pathinfo($file, PATHINFO_EXTENSION));
        $fileSize = $_FILES["inputImage"]["size"];

        if($fileSize > 10000000){
            echo "Maximum file size of 10mb exceeded."."</br>";
            $error = 1;
        }

        if($fileType != "jpg" && $fileType != "png" && $fileType != "jpeg"){
            echo "File format not supported.";
            $error = 1;
        }

        if($error == 0){
            move_uploaded_file($_FILES["inputImage"]["tmp_name"], $file);
            if (!file_exists($file)) {
                echo "Image not successfully uploaded.";
            }
            else{
                echo "Image successfully uploaded.";

                $conn = mysqli_connect("localhost", "root", "", "training_db") or die ("Database Error!"); 
                $sql = "UPDATE user_info_tbl SET uit_image = '".$newFilename."' WHERE uit_id=".$index;
                if(mysqli_query($conn, $sql)){
                    header("Refresh: 0");
                }
                else{
                    echo "<p style='color:red;'>Something went wrong.</p>";
                }
                mysqli_close($conn);
            }
        }

        



    }
?>