-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 09, 2021 at 10:11 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.3.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `training_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `user_info_tbl`
--

CREATE TABLE `user_info_tbl` (
  `uit_id` int(11) NOT NULL,
  `uit_firstname` varchar(100) NOT NULL,
  `uit_midname` varchar(100) NOT NULL,
  `uit_lastname` varchar(100) NOT NULL,
  `uit_birthdate` date NOT NULL,
  `uit_age` int(11) NOT NULL,
  `uit_address` varchar(250) NOT NULL,
  `uit_email` varchar(100) NOT NULL,
  `uit_username` varchar(100) NOT NULL,
  `uit_password` varchar(100) NOT NULL,
  `uit_image` varchar(500) NOT NULL,
  `uit_added_on` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_info_tbl`
--

INSERT INTO `user_info_tbl` (`uit_id`, `uit_firstname`, `uit_midname`, `uit_lastname`, `uit_birthdate`, `uit_age`, `uit_address`, `uit_email`, `uit_username`, `uit_password`, `uit_image`, `uit_added_on`) VALUES
(1, 'Junaina', 'Romblon', 'Manangan', '1999-02-06', 22, 'Taguig City', 'sample@email.com', 'junsample', 'SnVuMTIzNDU=', 'assets/(09092021095926)chibi_20210906_171631.png', '2021-09-09 00:00:00'),
(2, 'Sample FName', 'Sample MName', 'Sample LName', '1998-02-06', 22, 'Taguig City', 'sample_again@email.com', 'Sample123', 'U2FtcGxlMTIz', 'assets/(09092021100145)chibi_20210906_171631 - Copy.png', '2021-09-09 00:00:00'),
(3, 'Junaina', '', 'Manangan', '2021-09-05', 0, 'South Daang Hari, Taguig City', 'junainamanangan@gmail.com', 'test1234', 'VGVzdDEyMzQ=', 'assets/person-icon.png', '2021-09-09 15:04:32'),
(4, 'Junaina', '', 'Manangan', '2021-09-06', 0, 'South Daang Hari, Taguig City', 'junainamanangan@gmail.com', 'testagain1234', 'dGVzVDEyMzQ=', 'assets/person-icon.png', '2021-09-09 16:02:14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user_info_tbl`
--
ALTER TABLE `user_info_tbl`
  ADD PRIMARY KEY (`uit_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user_info_tbl`
--
ALTER TABLE `user_info_tbl`
  MODIFY `uit_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
