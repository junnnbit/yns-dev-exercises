<?php
   session_start();
   unset($_SESSION["username"]);
   unset($_SESSION["password"]);

   unset($_SESSION['firstname']);
   unset($_SESSION['middlename']);
   unset($_SESSION['lastname']);
   unset($_SESSION['birthdate']);
   unset($_SESSION['age']);
   unset($_SESSION['address']);
   unset($_SESSION['email']);
   unset($_SESSION['username']);
   unset($_SESSION['password']);
   unset($_SESSION['image']);

   session_destroy();
   
   header('Location: 3-5_login.php');
?>