<style>
    .body-style{
        background-color: #e6f2ff
    }
    .login-form{
        height: 50%;
        width:  50%;
        border: 3px solid blue;
        margin-left: auto;
        margin-right: auto;
        margin-top:15%;
    }
    .center{
        margin-left: auto;
        margin-right: auto;
        align-items: center;
    }
    .title-style{
        text-align: center;
    }
    input{
        height: 30px;
        border-radius: 7px;
    }
</style>
<html>
    <head></head>
    <body class="body-style">
        <form method="post">
            <div class="login-form">
            <div style="padding-top:20px;"></div>
                <h2 class="title-style">USER'S LOGIN</h2>
                <div style="padding-top:20px;"></div>
                <table class="center">
                    <tr>
                        <td>Username:</td>
                        <td><input type="text" name="username_login" required></td>
                    </tr>
                    <tr>
                        <td>Password:</td>
                        <td><input type="password" name="username_password" required></td>
                    </tr>
                </table>
                <br/><br/>
                <div class="center" style="text-align:center;">
                    <button type="submit" name="submitButton" style="padding:7px 15px;">Login</button>
                </div>
            </div>
        </form>
    </body>
</html>
<script>
if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
}
</script>

<?php
    if(isset($_POST['submitButton'])){
        $username = $_POST['username_login'];
        $password = $_POST['username_password'];

        $conn = mysqli_connect("localhost", "root", "", "training_db") or die ("Database Error!"); 

        if(!empty($username) && !empty($password)){
            $sql = "SELECT * FROM user_info_tbl WHERE uit_username='".$username."' AND uit_password='".base64_encode($password)."'";
            $result = mysqli_query($conn, $sql);
            if($result == false){
                echo "<p style='text-align:center;color:red;'>Error in query.</p>";
            }
            else{
                $row = mysqli_fetch_array($result);
                if(!empty($row)){
                    session_start();
                    $_SESSION['firstname'] = $row['uit_firstname'];
                    $_SESSION['middlename'] = $row['uit_midname'];
                    $_SESSION['lastname'] = $row['uit_lastname'];
                    $_SESSION['birthdate'] = $row['uit_birthdate'];
                    $_SESSION['age'] = $row['uit_age'];
                    $_SESSION['address'] = $row['uit_address'];
                    $_SESSION['email'] = $row['uit_email'];
                    $_SESSION['username'] = $row['uit_username'];
                    $_SESSION['password'] = $row['uit_password'];
                    $_SESSION['image'] = "../".$row['uit_image'];
                    header('Location: 3-5_mainPage.php');
                }
                else{
                    echo "<p style='text-align:center;color:red;'>Username or Password is incorrect.</p>";
                }

            }
        } 
        else{
            echo "<p style='text-align:center;color:red;'>Please fill out the fields.</p>";
        } 
        mysqli_close($conn);
    }
?>