-- 1) Retrieve employees whose last name start with "K".
SELECT * FROM employees WHERE last_name like 'K%';

-- 2) Retrieve employees whose last name end with "i".
SELECT * FROM employees WHERE last_name like '%i';

-- 3) Retrieve employee's full name and their hire date whose hire date is between 2015/1/1 and 2015/3/31 ordered by ascending by hire date. (Hint. Use CONCAT)
SELECT CONCAT(first_name, ' ', middle_name, ' ', last_name) as fullname, hire_date 
FROM employees WHERE hire_date 
BETWEEN '2015/1/1'
AND '2015/3/31' ORDER BY hire_date;

-- 4) Retrieve employee's last name and their boss's last name. If they don't have boss, no need to retrieve and show.
SELECT emp.last_name as Employee, boss.last_name as Boss 
FROM employees as emp
INNER JOIN employees as boss
ON boss.id = emp.boss_id;

-- 5) Retrieve employee's last name who belong to Sales department ordered by descending by last name.
SELECT emp.last_name
FROM employees as emp
INNER JOIN departments dept
ON emp.department_id = dept.id
WHERE
dept.name = 'Sales'
ORDER BY emp.last_name DESC;

-- 6) Retrieve number of employee who has middle name.
SELECT COUNT(*) AS count_has_middle FROM employees 
WHERE middle_name IS NOT NULL AND middle_name <> '';

-- 7) Retrieve department name and number of employee in each department. You don't need to retrieve the department name which doesn't have employee.
SELECT UPPER(dept.name) as name, COUNT(*) as employees_count 
FROM departments AS dept 
INNER JOIN employees AS emp 
ON emp.department_id = dept.id GROUP BY dept.id;

-- 8) Retrieve employee's full name and hire date who was hired the most recently.
SELECT first_name, middle_name, last_name, hire_date 
FROM employees
ORDER BY hire_date DESC
LIMIT 1;

-- 9) Retrieve department name which has no employee.
SELECT UPPER(name) 
FROM departments AS dept 
WHERE NOT EXISTS (SELECT DISTINCT department_id FROM employees AS e WHERE dept.id = e.department_id);

-- 10) Retrieve employee's full name who has more than 2 positions 
SELECT emp.first_name, emp.middle_name, emp.last_name
FROM employees as emp
INNER JOIN
employee_positions as emp_pos
ON emp.id = emp_pos.employee_id
GROUP BY employee_id
HAVING COUNT(employee_id) > 1;