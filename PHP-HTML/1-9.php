<style>
    table{
        width: 100%;
    }

    table, th, td{
        border: 1px solid black;
    }
</style>

<html>
    <head></head>

    <body>
        <h2 style="text-align: center; padding-top: 20px;">Users List</h2>
        <table class="table">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Middle Name</th>
                    <th>Last Name</th>
                    <th>Birtdate</th>
                    <th>Age</th>
                    <th>Address</th>
                    <th>Email</th>
                    <th>Username</th>
                    <th>Password</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if(file_exists("users.csv")){
                        $csvfile = 'users.csv';
                        $file_handle = fopen($csvfile, 'r');
                        $index = 0;
                        while(!feof($file_handle))
                        {
                ?>
                <tr>
                <?php      
                            $line_data = fgetcsv($file_handle, 1024);
                            if(!empty($line_data)){
                                $index2 = 0;
                                foreach($line_data as $data){
                                    if($index2 != 9){
                ?>
                    <td><?php echo $data;?></td>
                <?php   
                                    }
                                    $index2++;
                                }    
                ?><?php
                                $index++;
                            }
                ?>
                </tr>
                <?php
                        }
                        fclose($file_handle);
                    }
                    else{

                ?>
                <tr>
                    <td style="text-align: center;" colspan="10">No data found.</td>
                </tr>
                <?php
                    }
                ?>
            </tbody>
        </table>
    </body>
</html>

<script>
if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
}

</script>