<html>
    <head></head>

    <body>
        <form method="post" action="1-6_outputPage.php">
            <h3>User Basic Information Form:</h3>
            <label for="inputFirstNameID"> First Name: </label>
            <input type="text" id="inputFirstNameID" name="inputFirstName" required>
            <br/><br/>
            <label for="inputMidNameID"> Middle Name: </label>
            <input type="text" id="inputMidNameID" name="inputMidName">
            <br/><br/>
            <label for="inputLastNameID"> Last Name: </label>
            <input type="text" id="inputLastNameID" name="inputLastName" required>
            <br/><br/>
            <label for="inputBirthdateID"> Birthdate: </label>
            <input type="date" id="inputBirthdateID" name="inputBirthdate" onchange="ageCalc()" required>
            <br/><br/>
            <label for="inputAgeID"> Age: </label>
            <input type="text" id="inputAgeID" name="inputAge" readonly>
            <br/><br/>
            <label for="inputAddressID"> Address: </label>
            <input type="text" id="inputAddressID" name="inputAddress" required>
            <br/><br/>
            <hr>
            <br/><br/>
            <label for="inputEmailID"> Email: </label>
            <input type="text" id="inputEmailID" name="inputEmail" required>
            <br/><br/>
            <label for="inputUserNameID"> Username: </label>
            <input type="text" id="inputUserNameID" name="inputUserName" required>
            <br/><br/>
            <label for="inputUserPassID"> Password: </label>
            <input type="password" id="inputUserPassID" name="inputUserPass" required>
            <br/><br/><br/>
            <button type="submit" name="submitButton">Submit</button>
        </form>
    </body>
</html>

<script>
if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
}

function ageCalc(){
    var birthdateInput = document.getElementById("inputBirthdateID").value;

    var dateToday = new Date();
    var birthDate = new Date(birthdateInput);
    var age = dateToday.getFullYear() - birthDate.getFullYear();
    var monthDiff = dateToday.getMonth() - birthDate.getMonth();
    if(monthDiff < 0 || (monthDiff === 0 && dateToday.getDate() < birthDate.getDate())) {
        age--;
    }

    document.getElementById("inputAgeID").value = age;
}
</script>