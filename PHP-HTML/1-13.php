<style>
    .body-style{
        background-color: #e6f2ff
    }
    .login-form{
        height: 50%;
        width:  50%;
        border: 3px solid blue;
        margin-left: auto;
        margin-right: auto;
        margin-top:15%;
    }
    .center{
        margin-left: auto;
        margin-right: auto;
        align-items: center;
    }
    .title-style{
        text-align: center;
    }
    input{
        height: 30px;
        border-radius: 7px;
    }
</style>
<html>
    <head></head>
    <body class="body-style">
        <form method="post">
            <div class="login-form">
            <div style="padding-top:20px;"></div>
                <h2 class="title-style">USER'S LOGIN</h2>
                <div style="padding-top:20px;"></div>
                <table class="center">
                    <tr>
                        <td>Username:</td>
                        <td><input type="text" name="username_login" required></td>
                    </tr>
                    <tr>
                        <td>Password:</td>
                        <td><input type="password" name="username_password" required></td>
                    </tr>
                </table>
                <br/><br/>
                <div class="center" style="text-align:center;">
                    <button type="submit" name="submitButton" style="padding:7px 15px;">Login</button>
                </div>
            </div>
        </form>
    </body>
</html>
<script>
if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
}
</script>

<?php
    if(isset($_POST['submitButton'])){
        $username = $_POST['username_login'];
        $password = $_POST['username_password'];

        if(!empty($username) && !empty($password)){

            if(file_exists("users.csv")){
                $csvfile = 'users.csv';
                $file_handle = fopen($csvfile, 'r');
                $found = 0;
                $data = array();
                
                while(!feof($file_handle))
                {
                    $line_data = fgetcsv($file_handle, 1024);
                    if(!empty($line_data)){
                        if(($username == $line_data[7]) && ($password == base64_decode($line_data[8]))){
                            $found = 1;
                            array_push($data, $line_data);
                            break;
                        }
                    }
                }
                fclose($file_handle);

                if($found == 1){
                    session_start();
                    $_SESSION['firstname'] = $data[0][0];
                    $_SESSION['middlename'] = $data[0][1];
                    $_SESSION['lastname'] = $data[0][2];
                    $_SESSION['birthdate'] = $data[0][3];
                    $_SESSION['age'] = $data[0][4];
                    $_SESSION['address'] = $data[0][5];
                    $_SESSION['email'] = $data[0][6];
                    $_SESSION['username'] = $data[0][7];
                    $_SESSION['password'] = $data[0][8];
                    $_SESSION['image'] = $data[0][9];
                    header('Location: 1-13_mainPage.php');
                }
                else{
                    echo "<p style='text-align:center;color:red;'>Username or Password is incorrect.</p>";
                }
            }  
        } 
        else{
            echo "<p style='text-align:center;color:red;'>Please fill out the fields.</p>";
        } 
    }
?>