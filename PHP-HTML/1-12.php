<?php
    if(file_exists("users.csv")){
        $csvfile = 'users.csv';
        $file_handle = fopen($csvfile, 'r');
        $list_copy = array();
        
        while(!feof($file_handle))
        {
            $line_data = fgetcsv($file_handle, 1024);
            if(!empty($line_data)){
                array_push($list_copy, $line_data);
            }
        }
        fclose($file_handle);

        $counter = 0;
        $array_main = array();
        $array_sub = array();
        $list_count = count($list_copy);

        for($i=0; $i<$list_count; $i++){
            array_push($list_copy[$i], $i);
            $lc = $list_copy[$i];
            if($i != ($list_count-1)){
                if($counter < 9){
                    array_push($array_sub, $lc);
                    $counter++;
                }
                else{
                    array_push($array_sub, $lc);
                    array_push($array_main, $array_sub);
                    $array_sub = array();
                    $counter = 0;
                }
            }
            else{
                if($counter < 9){
                    array_push($array_sub, $lc);
                    array_push($array_main, $array_sub);
                }
                else{
                    array_push($array_sub, $lc);
                    array_push($array_main, $array_sub);
                    $counter = 0;
                }
            }
        }
        
        $page = !empty($_GET['page']) ? (int) $_GET['page'] : 1;
        // echo json_encode($array_main);
        $data = $array_main[$page-1];
        // exit;
    }
    

?>

<style>
    /* Table */
    table{
        width: 100%;
    }

    table, th, td{
        border: 1px solid black;
    }

    /* Pagination links */
    .pagination {
        padding-top: 20px;
    }
    .pagination a {
        color: black;
        float: left;
        padding: 8px 16px;
        text-decoration: none;
        transition: background-color .3s;
    }

    /* Style the active/current link */
    .pagination a.active {
        background-color: dodgerblue;
        color: white;
    }

    /* Add a grey background color on mouse-over */
    .pagination a:hover:not(.active) {background-color: #ddd;}

    /* Buttons cursor */
    button{
        cursor: pointer;
    }
</style>

<html>
    <head></head>

    <body>
        <h2 style="text-align: center; padding-top: 20px;">Users List</h2>
        <button style="float:right;padding:10px;background-color: #4CAF50;" onClick="addBtn()">Add New User</button>
        <div style="padding-top:40px;padding-bottom:40px;"></div>
        <table class="table">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Middle Name</th>
                    <th>Last Name</th>
                    <th>Birtdate</th>
                    <th>Age</th>
                    <th>Address</th>
                    <th>Email</th>
                    <th>Username</th>
                    <th>Password</th>
                    <th>Image</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    for($i=0;$i<count($data);$i++){
                        $d = $data[$i];
                ?>
                        <tr>
                <?php
                        for($j=0;$j<count($d);$j++){
                            $disp_data = $d[$j];
                            if($j == 9){
                ?>
                                <td style="text-align:center;"><img src="<?php echo $disp_data;?>" style="height: 50px; width: 50px;"></td>
                <?php
                            }
                            else if($j == 10){}
                            else{
                ?>
                                <td><?php echo $disp_data;?></td>
                <?php
                            }
                        }
                ?>
                        <td><button name="openBtn" onClick="redirect(<?php echo $d[10];?>)">Open Info</button></td>
                    </tr>
                <?php
                    }
                ?>
            </tbody>
        </table>
        <div class="pagination">
            <div style="float:left;">
                <?php echo "Page ".$page." of ".count($array_main);?>
            </div>
            <div style="float:right;">
                <?php
                    if($page == 1){
                ?>
                        <a href="#">&laquo;</a>
                <?php    
                    }
                    else{
                ?>
                        <a href="<?php echo "1-12.php?page=1";?>">&laquo;</a>
                <?php
                    }
                ?>
                <?php 
                    for($i=1; $i<(count($array_main)+1); $i++){
                        if($i == $page){
                ?>
                            <a class="active" href="#"><?php echo $i?></a>
                <?php
                        }
                        else{
                ?>
                            <a href="<?php echo "1-12.php?page=".$i;?>"><?php echo $i?></a>
                <?php
                        }
                    }
                ?>
                <?php
                    if($page == count($array_main)){
                ?>
                        <a href="#">&raquo;</a>
                <?php    
                    }
                    else{
                ?>
                        <a href="<?php echo "1-12.php?page=".count($array_main);?>">&raquo;</a>
                <?php
                    }
                ?>
            </div>
        </div>
    </body>
</html>

<script>
if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
}

// var button = document.getElementByName("openBtn");
// button.addEventListener('click', function() {
//   document.location.href = '1-10_form.php';
// });
function redirect(index){
    document.location.href = '1-12_form.php?index='+index;
}

function addBtn(){
    document.location.href = '1-8.php';
}
</script>
