<html>
    <head></head>

    <body>
        <form method="post">
            <label for="inputNum1Id"> First Number: </label>
            <input type="text" id="inputNum1Id" name="inputNum1" required>
            <label for="inputNum2Id"> Second Number: </label>
            <input type="text" id="inputNum2Id" name="inputNum2" required>
            <br/><br/>
            <button type="submit" value="Add" name="submitButton">Add</button>
            <button type="submit" value="Subtract" name="submitButton">Subtract</button>
            <button type="submit" value="Multiply" name="submitButton">Multiply</button>
            <button type="submit" value="Divide" name="submitButton">Divide</button>
        </form>
    </body>
</html>

<script>
if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
}
</script>
<?php
    if(isset($_POST['submitButton'])){
        $num1 = $_POST['inputNum1'];
        $num2 = $_POST['inputNum2'];
        $action = $_POST['submitButton'];

        if($action == "Add"){
            $result = $num1 + $num2;
            $message = "The sum of ".$num1." and ".$num2." is ".$result;
        }
        else if($action == "Subtract"){
            $result = $num1 - $num2;
            $message = "The difference of ".$num1." and ".$num2." is ".$result;
        }
        else if($action == "Multiply"){
            $result = $num1 * $num2;
            $message = "The product of ".$num1." and ".$num2." is ".$result;
        }
        else if($action == "Divide"){
            $result = $num1 / $num2;
            $message = "The quotient of ".$num1." and ".$num2." is ".$result;
        }
        echo "First Number: ".$num1."<br/>";
        echo "Second Number: ".$num2."<br/><br/>";
        echo $message;
    }
?>