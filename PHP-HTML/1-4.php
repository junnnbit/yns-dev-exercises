<html>
    <head></head>

    <body>
        <form method="post">
            <label for="inputNum1Id"> Number: </label>
            <input type="text" id="inputNum1Id" name="inputNum1" required>
            <br/><br/>
            <button type="submit" name="submitButton">Submit</button>
        </form>
    </body>
</html>

<script>
if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
}
</script>
<?php
    if(isset($_POST['submitButton'])){
        $num1 = $_POST['inputNum1'];

        echo "Max Number Inputted: ".$num1."<br/><br/>";

        for($i=1; $i<($num1+1); $i++){
            if(($i%3 == 0) && ($i%5 == 0)){
                echo "FizzBuzz"."<br/>";
            }
            else if($i%3 == 0){
                echo "Fizz"."<br/>";
            }
            else if($i%5 == 0){
                echo "Buzz"."<br/>";
            }
            else{
                echo $i."<br/>";
            }
        }

       
    }
?>