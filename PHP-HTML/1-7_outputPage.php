<?php
    session_start();
    $data = $_SESSION['submittedData'];

    //user basic info
    $firstname = $data['inputFirstName'];
    $middlename = $data['inputMidName'];
    $lastname = $data['inputLastName'];
    $birthdate = $data['inputBirthdate'];
    $age = $data['inputAge'];
    $address = $data['inputAddress'];

    //user credentials info
    $email = $data['inputEmail'];
    $username = $data['inputUserName'];
    $password = $data['inputUserPass'];

?>

<html>
    <head></head>

    <body>
        <form>
            <h3>User Basic Information:</h3>
            <label for="inputFirstNameID"> First Name: </label>
            <input type="text" id="inputFirstNameID" value="<?php echo $firstname;?>" readonly>
            <br/><br/>
            <label for="inputMidNameID"> Middle Name: </label>
            <input type="text" id="inputMidNameID" value="<?php echo $middlename;?>" readonly>
            <br/><br/>
            <label for="inputLastNameID"> Last Name: </label>
            <input type="text" id="inputLastNameID" value="<?php echo $lastname;?>" readonly>
            <br/><br/>
            <label for="inputBirthdateID"> Birthdate: </label>
            <input type="date" id="inputBirthdateID" value="<?php echo $birthdate;?>" readonly>
            <br/><br/>
            <label for="inputAgeID"> Age: </label>
            <input type="text" id="inputAgeID" value="<?php echo $age;?>" readonly>
            <br/><br/>
            <label for="inputAddressID"> Address: </label>
            <input type="text" id="inputAddressID" value="<?php echo $address;?>" readonly>
            <br/><br/>
            <hr>
            <br/><br/>
            <label for="inputEmailID"> Email: </label>
            <input type="text" id="inputEmailID" value="<?php echo $email;?>" readonly>
            <br/><br/>
            <label for="inputUserNameID"> Username: </label>
            <input type="text" id="inputUserNameID" value="<?php echo $username;?>" readonly>
            <br/><br/>
            <label for="inputUserPassID"> Password: </label>
            <input type="password" id="inputUserPassID" value="<?php echo $password;?>" readonly>
        </form>
    </body>
</html>

<?php
    unset($_SESSION['submittedData']);
    session_destroy();
?>
<script>
if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
}
</script>