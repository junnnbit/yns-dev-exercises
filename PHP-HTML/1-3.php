<html>
    <head></head>

    <body>
        <form method="post">
            <label for="inputNum1Id"> First Number: </label>
            <input type="text" id="inputNum1Id" name="inputNum1" required>
            <label for="inputNum2Id"> Second Number: </label>
            <input type="text" id="inputNum2Id" name="inputNum2" required>
            <br/><br/>
            <button type="submit" name="submitButton">Submit</button>
        </form>
    </body>
</html>

<script>
if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
}
</script>
<?php
    if(isset($_POST['submitButton'])){
        $num1Post = $_POST['inputNum1'];
        $num2Post = $_POST['inputNum2'];
        $gcd = 0;

        //assign to first number the lowest number between the two numbers
        if($num1Post > $num2Post){
            $temp = $num1Post;
            $num1 = $num2Post;
            $num2 = $temp;
        }
        else{
            $num1 = $num1Post;
            $num2 = $num2Post;
        }

        for($i=1; $i<($num1+1); $i++){
            if(($num1%$i == 0) && ($num2%$i == 0)){
                $gcd = $i;
            }
        }

        echo "First Number: ".$num1Post."<br/>";
        echo "Second Number: ".$num2Post."<br/><br/>";
        echo "The Greatest Common Divisor of ".$num1Post." and ".$num2Post." is ".$gcd;
    }
?>