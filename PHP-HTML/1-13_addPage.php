<?php
    session_start();
    if(empty($_SESSION['username']) && empty($_SESSION['password'])){
        echo "<p style='text-align:center;color:red;padding-top:50px;font-size:30px;'>Session not found. You cannot access this site.</p>";
        exit;
    }
?>
<style>
    /* Buttons cursor */
    button{
        cursor: pointer;
    }
</style>
<html>
    <head></head>

    <body>
        <form method="post">
            <h3>User Basic Information Form:</h3>
            <label for="inputFirstNameID"> First Name: </label>
            <input type="text" id="inputFirstNameID" name="inputFirstName" required>
            <br/><br/>
            <label for="inputMidNameID"> Middle Name: </label>
            <input type="text" id="inputMidNameID" name="inputMidName">
            <br/><br/>
            <label for="inputLastNameID"> Last Name: </label>
            <input type="text" id="inputLastNameID" name="inputLastName" required>
            <br/><br/>
            <label for="inputBirthdateID"> Birthdate: </label>
            <input type="date" id="inputBirthdateID" name="inputBirthdate" onchange="ageCalc()" required>
            <br/><br/>
            <label for="inputAgeID"> Age: </label>
            <input type="text" id="inputAgeID" name="inputAge" readonly>
            <br/><br/>
            <label for="inputAddressID"> Address: </label>
            <input type="text" id="inputAddressID" name="inputAddress" required>
            <br/><br/>
            <hr>
            <br/><br/>
            <label for="inputEmailID"> Email: </label>
            <input type="text" id="inputEmailID" name="inputEmail" required>
            <br/><br/>
            <label for="inputUserNameID"> Username: </label>
            <input type="text" id="inputUserNameID" name="inputUserName" required>
            <br/><br/>
            <label for="inputUserPassID"> Password: </label>
            <input type="password" id="inputUserPassID" name="inputUserPass" required>
            <br/><br/><br/>
            <button type="submit" name="submitButton">Submit</button>
        </form>
    </body>
</html>

<script>
if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
}

function ageCalc(){
    var birthdateInput = document.getElementById("inputBirthdateID").value;

    var dateToday = new Date();
    var birthDate = new Date(birthdateInput);
    var age = dateToday.getFullYear() - birthDate.getFullYear();
    var monthDiff = dateToday.getMonth() - birthDate.getMonth();
    if(monthDiff < 0 || (monthDiff === 0 && dateToday.getDate() < birthDate.getDate())) {
        age--;
    }

    document.getElementById("inputAgeID").value = age;
}
</script>

<?php
    if(isset($_POST['submitButton'])){
        //user basic info
        $firstname = $_POST['inputFirstName'];
        $middlename = $_POST['inputMidName'];
        $lastname = $_POST['inputLastName'];
        $birthdate = $_POST['inputBirthdate'];
        $age = $_POST['inputAge'];
        $address = $_POST['inputAddress'];

        //user credentials info
        $email = $_POST['inputEmail'];
        $username = $_POST['inputUserName'];
        $password = $_POST['inputUserPass'];

        //invalid inputs counter
        $invalidCount = 0;

        //message string
        $message = "";

        if(preg_match("/([%\$#\*@]+)/", htmlspecialchars($firstname))){
            $invalidCount += 1;
            $message .= "Invalid First Name."."<br/>";
        }

        if(preg_match("/([%\$#\*@]+)/", htmlspecialchars($middlename))){
            $invalidCount += 1;
            $message .= "Invalid Middle Name."."<br/>";
        }

        if(preg_match("/([%\$#\*@]+)/", htmlspecialchars($lastname))){
            $invalidCount += 1;
            $message .= "Invalid Last Name."."<br/>";
        }

        if(preg_match("/([%\$#\*@]+)/", htmlspecialchars($address))){
            $invalidCount += 1;
            $message .= "Invalid Address."."<br/>";
        }

        //email validation
        if(!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", htmlspecialchars($email))){
            $invalidCount += 1;
            $message .= "Invalid Email Address."."<br/>";
        }

        if(preg_match("/([%\$#\*@]+)/", htmlspecialchars($username))){
            $invalidCount += 1;
            $message .= "Invalid Username."."<br/>";
        }

        //password validation
        $uppercase = preg_match('@[A-Z]@', $password);
        $lowercase = preg_match('@[a-z]@', $password);
        $number    = preg_match('@[0-9]@', $password);

        if(!$uppercase || !$lowercase || !$number || strlen($password) < 8) {
            $invalidCount += 1;
            $message .= 'Password should be at least 8 characters in length and should include at least one upper case letter and one number.';
        }

        if($invalidCount != 0){
            echo "<p style='color:red;'>";
            echo $message;
            echo "</p>";
        }
        else{
            $data = array(
                    htmlspecialchars($firstname),
                    htmlspecialchars($middlename),
                    htmlspecialchars($lastname),
                    htmlspecialchars($birthdate),
                    htmlspecialchars($age),
                    // htmlspecialchars(str_replace(', ', ' ',$address)),
                    htmlspecialchars($address),
                    htmlspecialchars($email),
                    htmlspecialchars($username),
                    htmlspecialchars(base64_encode($password)),
                    "assets/person-icon.png"
                );

            if(file_exists("users.csv")){
                $file = fopen("users.csv","a");
            }
            else{
                $file = fopen("users.csv","w");
            }
            fputcsv($file,$data);
            if(fclose($file)){
                echo 'Form submitted.';
            }
            
        }
    }

?>