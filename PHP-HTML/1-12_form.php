<?php
    $index = $_GET['index'];
    $data = [];

    if(file_exists("users.csv")){
        $csvfile = 'users.csv';
        $file_handle = fopen($csvfile, 'r');
        $i = 0;

        while(!feof($file_handle))
        {
            $line_data = fgetcsv($file_handle, 1024);
            if(!empty($line_data)){
                if($index == $i){
                    $data = $line_data;
                    break;
                }
            }

            $i++;
        }
        fclose($file_handle);
?>

<html>
    <head></head>

    <body>
    <?php
        if(!empty($data)){
    ?>
        <form method="post" enctype="multipart/form-data">
            <div style="padding-left:50px;">
                <h3>User Basic Information:</h3>
            </div>
            <div style="padding-right:500px;">
                <img src="<?php echo $data[9];?>" style="float:right;width:200px;height:200px;border:1px solid black;">
            </div>
            <div style="padding-left:50px;">
                <label for="inputFirstNameID"> First Name: </label>
                <input type="text" id="inputFirstNameID" value="<?php echo $data[0];?>" readonly>
                <br/><br/>
                <label for="inputMidNameID"> Middle Name: </label>
                <input type="text" id="inputMidNameID" value="<?php echo $data[1];?>" readonly>
                <br/><br/>
                <label for="inputLastNameID"> Last Name: </label>
                <input type="text" id="inputLastNameID" value="<?php echo $data[2];?>" readonly>
                <br/><br/>
                <label for="inputBirthdateID"> Birthdate: </label>
                <input type="date" id="inputBirthdateID" value="<?php echo $data[3];?>" readonly>
                <br/><br/>
                <label for="inputAgeID"> Age: </label>
                <input type="text" id="inputAgeID" value="<?php echo $data[4];?>" readonly>
                <br/><br/>
                <label for="inputAddressID"> Address: </label>
                <input type="text" id="inputAddressID" value="<?php echo $data[5];?>" readonly>
                <br/><br/>
            </div>
            <hr>
            <div style="padding-left:50px;">
                <br/><br/>
                <label for="inputEmailID"> Email: </label>
                <input type="text" id="inputEmailID" value="<?php echo $data[6];?>" readonly>
                <br/><br/>
                <label for="inputUserNameID"> Username: </label>
                <input type="text" id="inputUserNameID" value="<?php echo $data[7];?>" readonly>
                <br/><br/>
                <label for="inputUserPassID"> Password: </label>
                <input type="password" id="inputUserPassID" value="<?php echo $data[8];?>" readonly>
                <br/><br/><br/>
                <label for="inputImageID"> Upload Image: </label>
                <input type="file" id="inputImageID" name="inputImage" accept="image/*">
                <br/><br/><br/>
                <button type="submit" name="submitButton">Submit</button>
            </div>
        </form>
    <?php
        }
        else{
    ?>
        <h2 style="text-align: center;">No data found.</h2>
    <?php
        }
    }
    else{
    ?>
        <h2 style="text-align: center;">File missing.</h2>
    <?php
    }
    ?>
    </body>
</html>

<script>
if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
}
</script>

<?php
    if(isset($_POST['submitButton'])){
        $error = 0;
        $root_filepath = $_SERVER['DOCUMENT_ROOT']."/yns-dev-exercises/PHP-HTML/";
        $file_folder = "assets";
        $filename = $_FILES["inputImage"]["name"];
        $newFilename = $file_folder."/"."(".date('mdYHis').")".basename($filename);
        $file = $root_filepath.$newFilename;
        $fileType = strtolower(pathinfo($file, PATHINFO_EXTENSION));
        $fileSize = $_FILES["inputImage"]["size"];

        if($fileSize > 10000000){
            echo "Maximum file size of 10mb exceeded."."</br>";
            $error = 1;
        }

        if($fileType != "jpg" && $fileType != "png" && $fileType != "jpeg"){
            echo "File format not supported.";
            $error = 1;
        }

        if($error == 0){
            move_uploaded_file($_FILES["inputImage"]["tmp_name"], $file);
            if (!file_exists($file)) {
                echo "Image not successfully uploaded.";
            }
            else{
                echo "Image successfully uploaded.";

                if(file_exists("users.csv")){
                    $csvfile = 'users.csv';
                    $file_handle = fopen($csvfile, 'r');
                    $i = 0;
        
                    $new_data = array();
            
                    while(!feof($file_handle))
                    {
                        $line_data = fgetcsv($file_handle, 1024);
                        if(!empty($line_data)){
                            if($index == $i){
                                $line_data[9] = $newFilename;
                                array_push($new_data, $line_data);
                            }
                            else{
                                array_push($new_data, $line_data);
                            }
                        }
            
                        $i++;
                    }
                    fclose($file_handle);
                    
                    $file = fopen("users.csv","w");
                    foreach($new_data as $nd){
                        fputcsv($file,$nd);
                    }
                    fclose($file);
                    header("Refresh:0");
                }
            }
        }

        



    }
?>