<?php
    $csvFile = 'users.csv';
    if (file_exists($csvFile)) {
        $fileHandle = fopen($csvFile, 'r');
        $listCopy = [];
        
        while (!feof($fileHandle)) {
            $lineData = fgetcsv($fileHandle);
            if (!empty($lineData)) {
                array_push($listCopy, $lineData);
            }
        }
        fclose($fileHandle);

        $counter = 0;
        $arrayMain = [];
        $arraySub = [];
        $listCount = count($listCopy);
        $maxColIndex = 9;

        for ($i = 0; $i < $listCount; $i++) {
            array_push($listCopy[$i], $i);
            $lc = $listCopy[$i];
            if ($i != ($listCount-1)) {
                if ($counter < $maxColIndex) {
                    array_push($arraySub, $lc);
                    $counter++;
                } else {
                    array_push($arraySub, $lc);
                    array_push($arrayMain, $arraySub);
                    $arraySub = [];
                    $counter = 0;
                }
            } else {
                if ($counter < $maxColIndex) {
                    array_push($arraySub, $lc);
                    array_push($arrayMain, $arraySub);
                } else {
                    array_push($arraySub, $lc);
                    array_push($arrayMain, $arraySub);
                    $counter = 0;
                }
            }
        }
    
        $firstPage = 1;
        $page = !empty($_GET['page']) ? (int) $_GET['page'] : $firstPage;
        $data = $arrayMain[$page-1];
    }
    

?>

<style>
    /* Table */
    table{
        width: 100%;
    }

    table, th, td{
        border: 1px solid black;
    }

    /* Pagination links */
    .pagination {
        padding-top: 20px;
    }
    .pagination a {
        color: black;
        float: left;
        padding: 8px 16px;
        text-decoration: none;
        transition: background-color .3s;
    }

    /* Style the active/current link */
    .pagination a.active {
        background-color: dodgerblue;
        color: white;
    }

    /* Add a grey background color on mouse-over */
    .pagination a:hover:not(.active) {background-color: #ddd;}

    /* Buttons cursor */
    button{
        cursor: pointer;
    }
</style>

<html>
    <head></head>

    <body>
        <h2 style="text-align: center; padding-top: 20px;">Users List</h2>
        <button style="float:right;padding:10px;background-color: #4CAF50;" onClick="addBtn()">Add New User</button>
        <div style="padding-top:40px;padding-bottom:40px;"></div>
        <table class="table">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Middle Name</th>
                    <th>Last Name</th>
                    <th>Birtdate</th>
                    <th>Age</th>
                    <th>Address</th>
                    <th>Email</th>
                    <th>Username</th>
                    <th>Password</th>
                    <th>Image</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    for ($i = 0; $i < count($data); $i++) {
                        $d = $data[$i];
                        $rowIndexData = 10;
                ?>
                        <tr>
                <?php
                        for ($j = 0; $j < count($d); $j++) {
                            $dispData = $d[$j];
                            $imageIndex = 9;
                            if ($j === $imageIndex) {
                ?>
                                <td style="text-align:center;"><img src="<?= htmlspecialchars($dispData) ?>" style="height: 50px; width: 50px;"></td>
                <?php
                            } elseif ($j != $rowIndexData) {
                ?>
                                <td><?= htmlspecialchars($dispData) ?></td>
                <?php
                            }
                        }
                ?>
                        <td><button name="openBtn" onClick="redirect(<?= htmlspecialchars($d[$rowIndexData]) ?>)">Open Info</button></td>
                    </tr>
                <?php
                    }
                ?>
            </tbody>
        </table>
        <div class="pagination">
            <div style="float:left;">
                <?= "Page " . $page . " of " . count($arrayMain) ?>
            </div>
            <div style="float:right;">
                <?php
                    if ($page === 1) {
                ?>
                        <a href="#">&laquo;</a>
                <?php    
                    } else {
                ?>
                        <a href="<?= "1-12.php?page=" . $firstPage ?>">&laquo;</a>
                <?php
                    }
                ?>
                <?php 
                    for ($i = 1; $i < (count($arrayMain)+1); $i++) {
                        if ($i === $page) {
                ?>
                            <a class="active" href="#"><?= $i?></a>
                <?php
                        } else {
                ?>
                            <a href="<?= "1-12.php?page=" . $i ?>"><?= $i?></a>
                <?php
                        }
                    }
                ?>
                <?php
                    if ($page == count($arrayMain)) {
                ?>
                        <a href="#">&raquo;</a>
                <?php    
                    } else {
                ?>
                        <a href="<?= "1-12.php?page=" . count($arrayMain) ?>">&raquo;</a>
                <?php
                    }
                ?>
            </div>
        </div>
    </body>
</html>

<script>
if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
}

function redirect(index){
    document.location.href = '1-12_form.php?index=' + index;
}

function addBtn(){
    document.location.href = '1-8.php';
}
</script>
