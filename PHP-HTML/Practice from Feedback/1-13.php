<style>
    .body-style{
        background-color: #e6f2ff
    }
    .login-form{
        height: 50%;
        width:  50%;
        border: 3px solid blue;
        margin-left: auto;
        margin-right: auto;
        margin-top:15%;
    }
    .center{
        margin-left: auto;
        margin-right: auto;
        align-items: center;
    }
    .title-style{
        text-align: center;
    }
    input{
        height: 30px;
        border-radius: 7px;
    }
</style>
<html>
    <head></head>
    <body class="body-style">
        <form method="post">
            <div class="login-form">
            <div style="padding-top:20px;"></div>
                <h2 class="title-style">USER'S LOGIN</h2>
                <div style="padding-top:20px;"></div>
                <table class="center">
                    <tr>
                        <td>Username:</td>
                        <td><input type="text" name="usernameInput" required></td>
                    </tr>
                    <tr>
                        <td>Password:</td>
                        <td><input type="password" name="passwordInput" required></td>
                    </tr>
                </table>
                <br/><br/>
                <div class="center" style="text-align:center;">
                    <button type="submit" name="submitButton" style="padding:7px 15px;">Login</button>
                </div>
            </div>
        </form>
    </body>
</html>
<script>
if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
}
</script>

<?php
    if (isset($_POST['submitButton'])) {
        $username = $_POST['usernameInput'];
        $password = $_POST['passwordInput'];

        if (!empty($username) && !empty($password)) {
            $csvFile = 'users.csv';
            if (file_exists($csvFile)) {
                $fileHandle = fopen($csvFile, 'r');
                $found = 0;
                $data = array();
                
                while (!feof($fileHandle)) {
                    $lineData = fgetcsv($fileHandle);
                    if (!empty($lineData)) {
                        if (($username == $lineData[7]) && ($password == base64_decode($lineData[8]))) {
                            $found = 1;
                            array_push($data, $lineData);
                            break;
                        }
                    }
                }
                fclose($fileHandle);

                if ($found == 1) {
                    session_start();
                    $dataIndexZero = $data[0];
                    $_SESSION['firstname'] = $dataIndexZero[0];
                    $_SESSION['middlename'] = $dataIndexZero[1];
                    $_SESSION['lastname'] = $dataIndexZero[2];
                    $_SESSION['birthdate'] = $dataIndexZero[3];
                    $_SESSION['age'] = $dataIndexZero[4];
                    $_SESSION['address'] = $dataIndexZero[5];
                    $_SESSION['email'] = $dataIndexZero[6];
                    $_SESSION['username'] = $dataIndexZero[7];
                    $_SESSION['password'] = $dataIndexZero[8];
                    $_SESSION['image'] = $dataIndexZero[9];
                    header('Location: 1-13_mainPage.php');
                } else {
                    echo '<p style="text-align:center;color:red;">Username or Password is incorrect.</p>';
                }
            }  
        } else {
            echo '<p style="text-align:center;color:red;">Please fill out the fields.</p>';
        } 
    }
?>