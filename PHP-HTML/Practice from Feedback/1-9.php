<style>
    table{
        width: 100%;
    }

    table, th, td{
        border: 1px solid black;
    }
</style>

<html>
    <head></head>

    <body>
        <h2 style="text-align: center; padding-top: 20px;">Users List</h2>
        <table class="table">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Middle Name</th>
                    <th>Last Name</th>
                    <th>Birtdate</th>
                    <th>Age</th>
                    <th>Address</th>
                    <th>Email</th>
                    <th>Username</th>
                    <th>Password</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $csvFile = 'users.csv';
                    if (file_exists($csvFile)) {
                        $fileHandle = fopen($csvFile, 'r');
                        $firstLoopIndex = 0;
                        while (!feof($fileHandle)) {
                ?>
                <tr>
                <?php      
                            $lineData = fgetcsv($fileHandle);
                            if (!empty($lineData)) {
                                $secondLoopIndex = 0;
                                $imageIndex = 9;
                                foreach ($lineData as $data) {
                                    if ($secondLoopIndex != $imageIndex) {
                ?>
                    <td><?= htmlspecialchars($data) ?></td>
                <?php   
                                    }
                                    $secondLoopIndex++;
                                }    
                ?><?php
                                $firstLoopIndex++;
                            }
                ?>
                </tr>
                <?php
                        }
                        fclose($fileHandle);
                    } else {

                ?>
                <tr>
                    <td style="text-align: center;" colspan="10">No data found.</td>
                </tr>
                <?php
                    }
                ?>
            </tbody>
        </table>
    </body>
</html>

<script>
if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
}

</script>