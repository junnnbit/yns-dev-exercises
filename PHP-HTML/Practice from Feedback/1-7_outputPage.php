<?php
    session_start();
    $data = $_SESSION['submittedData'];

    //user basic info
    $firstName = $data['inputFirstName'];
    $middleName = $data['inputMidName'];
    $lastName = $data['inputLastName'];
    $birthDate = $data['inputBirthdate'];
    $age = $data['inputAge'];
    $address = $data['inputAddress'];

    //user credentials info
    $email = $data['inputEmail'];
    $username = $data['inputUserName'];
    $password = $data['inputUserPass'];

?>

<html>
    <head></head>

    <body>
        <form>
            <h3>User Basic Information:</h3>
            <label for="first_name"> First Name: </label>
            <input type="text" id="first_name" value="<?= $firstName ?>" readonly>
            <br/><br/>
            <label for="mid_name"> Middle Name: </label>
            <input type="text" id="mid_name" value="<?= $middleName ?>" readonly>
            <br/><br/>
            <label for="last_name"> Last Name: </label>
            <input type="text" id="last_name" value="<?= $lastName ?>" readonly>
            <br/><br/>
            <label for="birthdate"> Birthdate: </label>
            <input type="date" id="birthdate" value="<?= $birthDate ?>" readonly>
            <br/><br/>
            <label for="age"> Age: </label>
            <input type="text" id="age" value="<?= $age ?>" readonly>
            <br/><br/>
            <label for="address"> Address: </label>
            <input type="text" id="address" value="<?= $address ?>" readonly>
            <br/><br/>
            <hr>
            <br/><br/>
            <label for="email"> Email: </label>
            <input type="text" id="email" value="<?= $email ?>" readonly>
            <br/><br/>
            <label for="username"> Username: </label>
            <input type="text" id="username" value="<?= $username ?>" readonly>
            <br/><br/>
            <label for="pass"> Password: </label>
            <input type="password" id="pass" value="<?= $password ?>" readonly>
        </form>
    </body>
</html>

<?php
    unset($_SESSION['submittedData']);
    session_destroy();
?>

<script>
if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
}
</script>