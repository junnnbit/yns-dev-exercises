<html>
    <head></head>

    <body>
        <form method="post">
            <label for="inputDateId"> Number: </label>
            <input type="date" id="inputDateId" name="inputDate" required>
            <br/><br/>
            <button type="submit" name="submitButton">Submit</button>
        </form>
    </body>
</html>

<script>
if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
}
</script>
<?php
    if (isset($_POST['submitButton'])) {
        $dateInputted = $_POST['inputDate'];

        if ($dateInputted != null && $dateInputted != '') {
            $dateTime = DateTime::createFromFormat($format = 'Y-m-d', $dateInputted);
            
            if ($dateTime && $dateTime->format($format) === $dateInputted) {
                $dateFormatted = date('F d, Y', strtotime($dateInputted));
                $datePlusThree = date('F d, Y', strtotime($dateInputted . ' + 3 days'));

                echo 'Day inputted: ' . $dateFormatted . '<br/><br/>';
                echo 'Three days from date inputted: ' . $datePlusThree . ' (' . date('l', strtotime($datePlusThree)) . ', Day ' . (intval(date('w', strtotime($datePlusThree))) + 1) . ' of the week starting from Sunday.)' . '<br/>';
            } else {
                echo 'Invalid date.';
            }
        } else {
            echo 'Please fill-out empty field/s.';
        }  
    }
?>