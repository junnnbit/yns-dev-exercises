<?php
    //user basic info
    $firstName = $_POST['inputFirstName'];
    $middleName = $_POST['inputMidName'];
    $lastName = $_POST['inputLastName'];
    $birthDate = $_POST['inputBirthdate'];
    $age = $_POST['inputAge'];
    $address = $_POST['inputAddress'];

    //user credentials info
    $email = $_POST['inputEmail'];
    $username = $_POST['inputUserName'];
    $password = $_POST['inputUserPass'];
?>

<html>
    <head></head>

    <body>
        <form>
            <h3>User Basic Information:</h3>
            <label for="first_name"> First Name: </label>
            <input type="text" id="first_name" value="<?= htmlspecialchars($firstName) ?>" readonly>
            <br/><br/>
            <label for="mid_name"> Middle Name: </label>
            <input type="text" id="mid_name" value="<?= htmlspecialchars($middleName) ?>" readonly>
            <br/><br/>
            <label for="last_name"> Last Name: </label>
            <input type="text" id="last_name" value="<?= htmlspecialchars($lastName) ?>" readonly>
            <br/><br/>
            <label for="birthdate"> Birthdate: </label>
            <input type="date" id="birthdate" value="<?= htmlspecialchars($birthDate) ?>" readonly>
            <br/><br/>
            <label for="age"> Age: </label>
            <input type="text" id="age" value="<?= htmlspecialchars($age) ?>" readonly>
            <br/><br/>
            <label for="address"> Address: </label>
            <input type="text" id="address" value="<?= htmlspecialchars($address) ?>" readonly>
            <br/><br/>
            <hr>
            <br/><br/>
            <label for="email"> Email: </label>
            <input type="text" id="email" value="<?= htmlspecialchars($email) ?>" readonly>
            <br/><br/>
            <label for="username"> Username: </label>
            <input type="text" id="username" value="<?= htmlspecialchars($username) ?>" readonly>
            <br/><br/>
            <label for="pass"> Password: </label>
            <input type="password" id="pass" value="<?= htmlspecialchars($password) ?>" readonly>
        </form>
    </body>
</html>

<script>
if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
}
</script>