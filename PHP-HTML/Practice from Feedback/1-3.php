<html>
    <head></head>

    <body>
        <form method="post">
            <label for="inputNum1Id"><small style="color:red;">*</small> First Number: </label>
            <input type="text" id="inputNum1Id" name="inputNum1" required>
            <label for="inputNum2Id"><small style="color:red;">*</small> Second Number: </label>
            <input type="text" id="inputNum2Id" name="inputNum2" required>
            <br/><br/>
            <button type="submit" name="submitButton">Submit</button>
        </form>
    </body>
</html>

<script>
if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
}
</script>

<?php
    if (isset($_POST['submitButton'])) {
        $firstNum = htmlspecialchars($_POST['inputNum1']);
        $secondNum = htmlspecialchars($_POST['inputNum2']);

        if (($firstNum != null && $firstNum != '') && ($secondNum != null && $secondNum != '')) {
            if (is_numeric($firstNum) && is_numeric($secondNum)) {
                $gcd = 0;

                $maxNumber = ($firstNum > $secondNum ? $secondNum : $firstNum) + 1;

                for ($i = 1; $i < $maxNumber; $i++) {
                    if ($firstNum%$i === 0 && $secondNum%$i === 0) {
                        $gcd = $i;
                    }
                }

                echo 'First Number: ' . $firstNum . '; Second Number: ' . $secondNum . '; The Greatest Common Divisor of ' . $firstNum . ' and ' . $secondNum . ' is ' . $gcd;
            } else {
                echo 'Inputs should be in numeric values. Please check your inputs.';
            }
        } else {
            echo 'Please fill-out empty field/s.';
        } 
    }
?>