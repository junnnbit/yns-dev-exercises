<html>
    <head></head>

    <body>
        <form method="post">
            <h3>User Basic Information Form:</h3>
            <label for="first_name"> First Name: </label>
            <input type="text" id="first_name" name="inputFirstName" required>
            <br/><br/>
            <label for="mid_name"> Middle Name: </label>
            <input type="text" id="mid_name" name="inputMidName">
            <br/><br/>
            <label for="last_name"> Last Name: </label>
            <input type="text" id="last_name" name="inputLastName" required>
            <br/><br/>
            <label for="birthdate"> Birthdate: </label>
            <input type="date" id="birthdate" name="inputBirthdate" onchange="ageCalc()" required>
            <br/><br/>
            <label for="age"> Age: </label>
            <input type="text" id="age" name="inputAge" readonly>
            <br/><br/>
            <label for="address"> Address: </label>
            <input type="text" id="address" name="inputAddress" required>
            <br/><br/>
            <hr>
            <br/><br/>
            <label for="email"> Email: </label>
            <input type="text" id="email" name="inputEmail" required>
            <br/><br/>
            <label for="username"> Username: </label>
            <input type="text" id="username" name="inputUserName" required>
            <br/><br/>
            <label for="pass"> Password: </label>
            <input type="password" id="pass" name="inputUserPass" required>
            <br/><br/><br/>
            <button type="submit" name="submitButton">Submit</button>
        </form>
    </body>
</html>

<script>
if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
}

function ageCalc(){
    var birthdateInput = document.getElementsByName("inputBirthdate")[0].value;

    var dateToday = new Date();
    var birthDate = new Date(birthdateInput);
    var age = dateToday.getFullYear() - birthDate.getFullYear();
    var monthDiff = dateToday.getMonth() - birthDate.getMonth();
    if(monthDiff < 0 || (monthDiff === 0 && dateToday.getDate() < birthDate.getDate())) {
        age--;
    }

    document.getElementsByName("inputAge")[0].value = age;
}
</script>

<?php
    if (isset($_POST['submitButton'])) {
        //user basic info
        $firstName = htmlspecialchars($_POST['inputFirstName']);
        $middleName = htmlspecialchars($_POST['inputMidName']);
        $lastName = htmlspecialchars($_POST['inputLastName']);
        $birthDate = htmlspecialchars($_POST['inputBirthdate']);
        $age = htmlspecialchars($_POST['inputAge']);
        $address = htmlspecialchars($_POST['inputAddress']);

        //user credentials info
        $email = htmlspecialchars($_POST['inputEmail']);
        $username = htmlspecialchars($_POST['inputUserName']);
        $password = htmlspecialchars($_POST['inputUserPass']);

        //invalid inputs counter
        $invalidCount = 0;

        //message string
        $message = '';

        //validators
        $noSpecialChars = '/([%\$#\*@]+)/';
        $emailValidator = '/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix';

        if (preg_match($noSpecialChars, $firstName)) {
            $invalidCount += 1;
            $message .= 'Invalid First Name.' . '<br/>';
        }

        if (preg_match($noSpecialChars, $middleName)) {
            $invalidCount += 1;
            $message .= 'Invalid Middle Name.' . '<br/>';
        }

        if (preg_match($noSpecialChars, $lastName)) {
            $invalidCount += 1;
            $message .= 'Invalid Last Name.' . '<br/>';
        }

        if (preg_match($noSpecialChars, $address)) {
            $invalidCount += 1;
            $message .= 'Invalid Address.' . '<br/>';
        }

        //email validation
        if (!preg_match($emailValidator, $email)) {
            $invalidCount += 1;
            $message .= 'Invalid Email Address.' . '<br/>';
        }

        if (preg_match($noSpecialChars, $username)) {
            $invalidCount += 1;
            $message .= 'Invalid Username.' . '<br/>';
        }

        //password validation
        $upperCaseRegEx = '@[A-Z]@';
        $lowerCaseRegEx = '@[a-z]@';
        $numberRegEx = '@[0-9]@';
        $minCount = 8;

        $upperCase = preg_match($upperCaseRegEx, $password);
        $lowerCase = preg_match($lowerCaseRegEx, $password);
        $number    = preg_match($numberRegEx, $password);

        if (!$upperCase || !$lowerCase || !$number || strlen($password) < $minCount) {
            $invalidCount += 1;
            $message .= 'Password should be at least 8 characters in length and should include at least one upper case letter and one number.';
        }

        if ($invalidCount != 0) {
            echo '<p style="color:red;">';
            echo $message;
            echo '</p>';
        } else {
            session_start();
            $_SESSION['submittedData'] = [
                                                'inputFirstName' => $firstName,
                                                'inputMidName' => $middleName,
                                                'inputLastName' => $lastName,
                                                'inputBirthdate' => $birthDate,
                                                'inputAge' => $age,
                                                'inputAddress' => $address,
                                                'inputEmail' => $email,
                                                'inputUserName' => $username,
                                                'inputUserPass' => $password,
                                        ];
            header('Location: 1-7_outputPage.php');
            exit();
        }
    }

?>