<?php
    $rowIndex = $_GET['index'];
    $data = [];

    $csvFile = 'users.csv';

    if (file_exists($csvFile)) {
        $fileHandle = fopen($csvFile, 'r');
        $i = 0;

        while (!feof($fileHandle)) {
            $lineData = fgetcsv($fileHandle);
            if (!empty($lineData)) {
                if ($rowIndex == $i) {
                    $data = $lineData;
                    break;
                }
            }
            $i++;
        }
        fclose($fileHandle);
?>

<html>
    <head></head>

    <body>
    <?php
        if (!empty($data)) {
    ?>
        <form method="post" enctype="multipart/form-data">
            <div style="padding-left:50px;">
                <h3>User Basic Information:</h3>
            </div>
            <div style="padding-right:500px;">
                <img src="<?= htmlspecialchars($data[9]) ?>" style="float:right;width:200px;height:200px;border:1px solid black;">
            </div>
            <div style="padding-left:50px;">
                <label for="inputFirstNameID"> First Name: </label>
                <input type="text" id="inputFirstNameID" value="<?= htmlspecialchars($data[0]) ?>" readonly>
                <br/><br/>
                <label for="inputMidNameID"> Middle Name: </label>
                <input type="text" id="inputMidNameID" value="<?= htmlspecialchars($data[1]) ?>" readonly>
                <br/><br/>
                <label for="inputLastNameID"> Last Name: </label>
                <input type="text" id="inputLastNameID" value="<?= htmlspecialchars($data[2]) ?>" readonly>
                <br/><br/>
                <label for="inputBirthdateID"> Birthdate: </label>
                <input type="date" id="inputBirthdateID" value="<?= htmlspecialchars($data[3]) ?>" readonly>
                <br/><br/>
                <label for="inputAgeID"> Age: </label>
                <input type="text" id="inputAgeID" value="<?= htmlspecialchars($data[4]) ?>" readonly>
                <br/><br/>
                <label for="inputAddressID"> Address: </label>
                <input type="text" id="inputAddressID" value="<?= htmlspecialchars($data[5]) ?>" readonly>
                <br/><br/>
            </div>
            <hr>
            <div style="padding-left:50px;">
                <br/><br/>
                <label for="inputEmailID"> Email: </label>
                <input type="text" id="inputEmailID" value="<?= htmlspecialchars($data[6]) ?>" readonly>
                <br/><br/>
                <label for="inputUserNameID"> Username: </label>
                <input type="text" id="inputUserNameID" value="<?= htmlspecialchars($data[7]) ?>" readonly>
                <br/><br/>
                <label for="inputUserPassID"> Password: </label>
                <input type="password" id="inputUserPassID" value="<?= htmlspecialchars($data[8]) ?>" readonly>
                <br/><br/><br/>
                <label for="inputImageID"> Upload Image: </label>
                <input type="file" id="inputImageID" name="inputImage" accept="image/*">
                <br/><br/><br/>
                <button type="submit" name="submitButton">Submit</button>
            </div>
        </form>
    <?php
        } else {
    ?>
        <h2 style="text-align: center;">No data found.</h2>
    <?php
        }
    } else {
    ?>
        <h2 style="text-align: center;">File missing.</h2>
    <?php
    }
    ?>
    </body>
</html>

<script>
if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
}
</script>

<?php
    if (isset($_POST['submitButton'])) {
        $error = 0;
        $rootFolder = '/yns-dev-exercises/PHP-HTML/Practice from Feedback/';
        $rootFilePath = $_SERVER['DOCUMENT_ROOT'] . $rootFolder;
        $fileFolder = 'assets';
        $fileName = $_FILES['inputImage']['name'];
        $dateUploaded = date('mdYHis');
        $newFileName = $fileFolder . '/' . '(' . $dateUploaded . ')' . basename($fileName);
        $file = $rootFilePath . $newFileName;
        $fileType = strtolower(pathinfo($file, PATHINFO_EXTENSION));
        $fileSize = $_FILES['inputImage']['size'];
        $maxFileSize = 10000000;
        $imageExtTypes = ['jpg', 'png', 'jpeg'];

        if ($fileSize > $maxFileSize) {
            echo 'Maximum file size of 10mb exceeded.' . '</br>';
            $error = 1;
        }

        if (!in_array($fileType, $imageExtTypes)) {
            echo 'File not in image format (jpg, png, jpeg).';
            $error = 1;
        }

        if ($error == 0) {
            move_uploaded_file($_FILES['inputImage']['tmp_name'], $file);
            if (!file_exists($file)) {
                echo 'Image not successfully uploaded.';
            } else {
                echo 'Image successfully uploaded.';
                $csvFile = 'users.csv';
                if (file_exists($csvFile)) {
                    $fileHandle = fopen($csvFile, 'r');
                    $i = 0;
                    $imageIndex = 9;

                    $newData = [];
            
                    while (!feof($fileHandle)) {
                        $lineData = fgetcsv($fileHandle);
                        if (!empty($lineData)) {
                            if ($rowIndex == $i) {
                                $lineData[$imageIndex] = $newFileName;
                                array_push($newData, $lineData);
                            } else {
                                array_push($newData, $lineData);
                            }
                        }
            
                        $i++;
                    }
                    fclose($fileHandle);
                    
                    $file = fopen($csvFile, 'w');
                    foreach ($newData as $nd) {
                        fputcsv($file, $nd);
                    }
                    fclose($file);
                    header('Refresh:0');
                }
            }
        }
    }
?>