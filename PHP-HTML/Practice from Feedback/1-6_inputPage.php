<html>
    <head></head>

    <body>
        <form method="post" action="1-6_outputPage.php">
            <h3>User Basic Information Form:</h3>
            <label for="first_name"> First Name: </label>
            <input type="text" id="first_name" name="inputFirstName" required>
            <br/><br/>
            <label for="mid_name"> Middle Name: </label>
            <input type="text" id="mid_name" name="inputMidName">
            <br/><br/>
            <label for="last_name"> Last Name: </label>
            <input type="text" id="last_name" name="inputLastName" required>
            <br/><br/>
            <label for="birthdate"> Birthdate: </label>
            <input type="date" id="birthdate" name="inputBirthdate" onchange="ageCalc()" required>
            <br/><br/>
            <label for="age"> Age: </label>
            <input type="text" id="age" name="inputAge" readonly>
            <br/><br/>
            <label for="address"> Address: </label>
            <input type="text" id="address" name="inputAddress" required>
            <br/><br/>
            <hr>
            <br/><br/>
            <label for="email"> Email: </label>
            <input type="text" id="email" name="inputEmail" required>
            <br/><br/>
            <label for="username"> Username: </label>
            <input type="text" id="username" name="inputUserName" required>
            <br/><br/>
            <label for="pass"> Password: </label>
            <input type="password" id="pass" name="inputUserPass" required>
            <br/><br/><br/>
            <button type="submit" name="submitButton">Submit</button>
        </form>
    </body>
</html>

<script>
if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
}

function ageCalc(){
    var birthdateInput = document.getElementsByName("inputBirthdate")[0].value;

    var dateToday = new Date();
    var birthDate = new Date(birthdateInput);
    var age = dateToday.getFullYear() - birthDate.getFullYear();
    var monthDiff = dateToday.getMonth() - birthDate.getMonth();
    if(monthDiff < 0 || (monthDiff === 0 && dateToday.getDate() < birthDate.getDate())) {
        age--;
    }

    document.getElementsByName("inputAge")[0].value = age;
}
</script>