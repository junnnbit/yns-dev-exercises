<html>
    <head></head>

    <body>
        <form method="post">
            <label for="inputNum1Id"><small style="color:red;">*</small> First Number: </label>
            <input type="text" id="inputNum1Id" name="inputNum1" required>
            <label for="inputNum2Id"><small style="color:red;">*</small> Second Number: </label>
            <input type="text" id="inputNum2Id" name="inputNum2" required>
            <br/><br/>
            <button type="submit" value="Add" name="submitButton">Add</button>
            <button type="submit" value="Subtract" name="submitButton">Subtract</button>
            <button type="submit" value="Multiply" name="submitButton">Multiply</button>
            <button type="submit" value="Divide" name="submitButton">Divide</button>
        </form>
    </body>
</html>

<script>
if (window.history.replaceState) {
        window.history.replaceState( null, null, window.location.href );
}
</script>

<?php
    if (isset($_POST['submitButton'])) {
        $firstNum = htmlspecialchars($_POST['inputNum1']);
        $secondNum = htmlspecialchars($_POST['inputNum2']);
        $action = $_POST['submitButton'];

        if (($firstNum != null && $firstNum != '') && ($secondNum != null && $secondNum != '')) {
            if (is_numeric($firstNum) && is_numeric($secondNum)) {
                switch ($action) {
                    case 'Add':
                        $result = $firstNum + $secondNum;
                        $message = 'The sum of ' . $firstNum . ' and ' . $secondNum . ' is ' . $result;
                        break;
                    case 'Subtract':
                        $result = $firstNum - $secondNum;
                        $message = 'The difference of ' . $firstNum . ' and ' . $secondNum . ' is ' . $result;
                        break;
                    case 'Multiply':
                        $result = $firstNum * $secondNum;
                        $message = 'The product of ' . $firstNum . ' and ' . $secondNum . ' is ' . $result;
                        break;
                    case 'Divide':
                        $result = $firstNum / $secondNum;
                        $message = 'The quotient of ' . $firstNum . ' and ' . $secondNum . ' is '. $result;
                        break;
                }
    
                echo 'First Number: ' . $firstNum . '; ' . 'Second Number: ' . $secondNum . '; ' . $message;
            } else {
                echo 'Inputs should be in numeric values. Please check your inputs.';
            }
        } else {
            echo 'Please fill-out empty field/s.';
        }  
    }
?>