<html>
    <head></head>

    <body>
        <form method="post">
            <label for="inputNum1Id"> Number: </label>
            <input type="text" id="inputNum1Id" name="inputNum1" required>
            <br/><br/>
            <button type="submit" name="submitButton">Submit</button>
        </form>
    </body>
</html>

<script>
if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
}
</script>

<?php
    if (isset($_POST['submitButton'])) {
        $firstNum = htmlspecialchars($_POST['inputNum1']);

        if ($firstNum != null && $firstNum != '') {
            if (is_numeric($firstNum)) {
                echo "Max Number Inputted: " . $firstNum . "<br/><br/>";

                for ($i = 1; $i < ($firstNum+1); $i++) {
                    if ($i%3 === 0 && $i%5 === 0) {
                        echo "FizzBuzz" . "<br/>";
                    } elseif ($i%3 == 0) {
                        echo "Fizz" . "<br/>";
                    } elseif ($i%5 == 0) {
                        echo "Buzz" . "<br/>";
                    } else {
                        echo $i . "<br/>";
                    }
                }
            } else {
                echo 'Inputs should be in numeric values. Please check your inputs.';
            }
        } else {
            echo 'Please fill-out empty field/s.';
        } 

       
    }
?>