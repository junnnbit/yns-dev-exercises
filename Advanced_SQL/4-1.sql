SELECT *, timestampdiff( YEAR, birth_date, now() ) AS age FROM employees
WHERE timestampdiff( YEAR, birth_date, now() ) > 30 AND timestampdiff( YEAR, birth_date, now() ) < 40;