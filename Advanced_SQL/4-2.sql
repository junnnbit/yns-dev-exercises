SELECT dws.therapist_id, t.name, dws.target_date, dws.start_time, dws.end_time, 
(CASE WHEN (dws.start_time <= '05:59:59' AND dws.start_time >= '00:00:00') 
THEN CONCAT(DATE_ADD(target_date, INTERVAL 1 DAY), " ", dws.start_time) 
ELSE CONCAT(target_date, " ", dws.start_time) END) as sort_start_time 
FROM daily_work_shifts as dws 
INNER JOIN therapists as t ON dws.therapist_id = t.id 
ORDER BY dws.target_date ASC, sort_start_time ASC;