-- CREATE QUERY
--CREATE therapists
CREATE TABLE therapists(
    id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100) NOT NULL
)
-- CREATE daily_work_shifts TABLE
CREATE TABLE daily_work_shifts(
    id  INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    therapist_id INT(11) NOT NULL,
    target_date DATE NOT NULL,
    start_time TIME NOT NULL,
    end_time TIME NOT NULL,
    INDEX(therapist_id)
)

-- INSERT QUERY
-- INSERT INTO therapists TABLE
INSERT INTO therapists (name) 
VALUES
('John'),
('Arnold'),
('Robert'),
('Ervin'),
('Smith');
-- INSERT INTO daily_work_shifts TABLE
INSERT INTO daily_work_shifts (therapist_id, target_date, start_time, end_time) 
VALUES
(1, '2021-09-09', '14:00:00', '15:00:00'),
(2, '2021-09-09', '22:00:00', '23:00:00'),
(3, '2021-09-09', '00:00:00', '01:00:00'),
(4, '2021-09-09', '05:00:00', '05:30:00'),
(1, '2021-09-09', '21:00:00', '21:45:00'),
(5, '2021-09-09', '05:30:00', '05:50:00'),
(3, '2021-09-09', '02:00:00', '02:30:00');

-- SELECT QUERY
SELECT dws.therapist_id, t.name, dws.target_date, dws.start_time, dws.end_time,
CASE 
    WHEN (dws.start_time <= '05:59:59' AND dws.start_time >= '00:00:00') THEN CONCAT(DATE_ADD(target_date, INTERVAL 1 DAY), ' ', dws.start_time) 
ELSE CONCAT(target_date, ' ', dws.start_time) 
END AS sort_start_time 
FROM daily_work_shifts AS dws 
INNER JOIN therapists AS t ON dws.therapist_id = t.id 
ORDER BY dws.target_date, sort_start_time;