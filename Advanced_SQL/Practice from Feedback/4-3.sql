SELECT emp.first_name, emp.middle_name, emp.last_name, emp.birth_date, emp.hire_date, pos.name 
FROM employees AS emp 
INNER JOIN employee_positions AS emp_pos 
ON emp.id = emp_pos.employee_id 
INNER JOIN 
(
    SELECT id, 
    CASE 
        WHEN name = 'CEO' THEN 'Chief Executive Officer' 
        WHEN name = 'CTO' THEN 'Chief Technical Officer' 
        WHEN name = 'CFO' THEN 'Chief Financial Officer' 
    ELSE name 
    END as name 
    FROM positions 
) AS pos 
ON emp_pos.position_id = pos.id;