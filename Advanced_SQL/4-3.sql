SELECT (CASE WHEN name = 'CEO' THEN 'Chief Executive Officer' 
WHEN name = 'CTO' THEN 'Chief Technical Officer' 
WHEN name = 'CFO' THEN 'Chief Financial Officer' 
ELSE name 
END) as name FROM positions;